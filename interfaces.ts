export interface User {
  uid: string;
  email: string;
  currentWorkspaceUID: string;
  name: string;
  profilePic: string;
  darkMode?: boolean;
}

export interface Workspace {
  uid: string;
  name: string;
  members: { [userID: string]: Member };
  created: number;
}

export interface Member {
  name: string;
  role: "owner" | "member";
  email: string;
  profilePic: string;
}

export interface Board {
  uid: string;
  owner: string;
  name: string;
  workspaceUID: string | null;
}

export interface Project {
  uid: string;
  name: string;
  bgColor: string;
  fontColor: string;
  totalTask: number;
  ownerUID: string;
  private?: boolean;
  categories: Category[];
  created: number;
}

export interface FormProject {
  name: string;
  bgColor: string;
  categories: Category[];
  fontColor: string;
  private?: boolean;
}

export interface Category {
  uid: string;
  name: string;
}

export interface Card {
  uid: string;
  title: string;
  tasks: Task[];
  categoryID: string;
  created: number;
  order?: number;
  // etc
}

export type CardUpdateType =
  | "tasksEdit"
  | "tasksCreate"
  | "tasksDelete"
  | "tasksOrder"
  | "tasksComplete"
  | "title"
  | "categoryID"
  | "cardAssignUser"
  | "assignedUserUID";

export interface Task {
  uid: string;
  name: string;
  completed: boolean;
  scheduledDayUID?: number;
  scheduledDayName?: string;
  assignedUserUID?: string;
}

export interface CalendarDay {
  dayNumber: number;
  dateNumber: number;
  monthNumber: number;
  yearNumber: number;
  uid: number;
  calendarCards: CalendarCard[];
}

export interface CalendarDayDB {
  uid: number;
  calendarCards: CalendarCard[];
}

export interface CalendarCard {
  projectUID: string;
  cardUID: string;
  task: Task;
  cardName: string;
  categoryID: string;
}

export interface CalendarDayDBList {
  [uid: string]: CalendarDayDB;
}

export interface ProjectList {
  [uid: string]: Project;
}

export interface CategoryList {
  [uid: string]: Category;
}

export interface InviteEmailData {
  to: string;
  from: string;
  workspace: string;
  inviteUID: string;
}

export interface InviteDBData {
  workspaceUID: string;
  workspaceName: string;
  from: string;
  deleted?: boolean;
}
