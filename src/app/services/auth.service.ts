import { Injectable, inject } from "@angular/core";
import {
  Auth,
  authState,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  UserCredential,
} from "@angular/fire/auth";
import {
  CollectionReference,
  Firestore,
  collection,
  doc,
  docData,
  setDoc,
  updateDoc,
} from "@angular/fire/firestore";
import {
  Storage,
  uploadBytesResumable,
  ref,
  getDownloadURL,
} from "@angular/fire/storage";
import { switchMap, tap, map, first } from "rxjs/operators";
import { of, Observable } from "rxjs";
import { Store } from "../store";
import { Router } from "@angular/router";
import { User, Workspace } from "../../../interfaces";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  UID: string = "";
  user$: Observable<User>;
  userCollection: CollectionReference<User>;
  workspaceCollection: CollectionReference<Workspace>;
  authState$: Observable<UserCredential["user"]>;

  constructor(
    private auth: Auth = inject(Auth),
    private fireStore: Firestore = inject(Firestore),
    private storage: Storage = inject(Storage),
    private store: Store,
    private router: Router
  ) {
    debugger;
    this.userCollection = collection(
      this.fireStore,
      "users"
    ) as CollectionReference<User>;
    this.workspaceCollection = collection(
      this.fireStore,
      "workspaces"
    ) as CollectionReference<Workspace>;

    this.authState$ = authState(this.auth);

    this.user$ = this.authState$.pipe(
      switchMap((user) => {
        if (user) {
          this.UID = user.uid;
          return docData(doc(this.userCollection, user.uid));
        } else {
          return of(null);
        }
      }),
      tap((user: User) => {
        if (user) {
          this.store.set("user", user);
        }
      })
    );
  }

  loginUser(email: string, password: string) {
    return signInWithEmailAndPassword(this.auth, email, password);
  }

  logout() {
    this.auth.signOut();
    this.router.navigateByUrl("auth/login");
  }

  async createUser(
    email: string,
    password: string,
    name: string,
    profilePic: File
  ) {
    // create account for user
    const credential = await createUserWithEmailAndPassword(
      this.auth,
      email,
      password
    );

    // add pic to storage
    const url = await this.uploadProfilePic(profilePic, credential.user.uid);

    // create first workspace
    const workspaceUID = doc(this.userCollection).id;
    await this.createFirstWorkspace(credential, workspaceUID, name, url);

    // set user data
    await this.setUserData(credential, name, url, workspaceUID);
  }

  private uploadProfilePic(file: File, userUID: string): Promise<string> {
    const path = `${userUID}/profilePic`;

    const storageRef = ref(this.storage, path);

    return new Promise(async (res, rej) => {
      try {
        const snap = await uploadBytesResumable(storageRef, file, {
          cacheControl: "max-age=31536000",
        });

        const url = await getDownloadURL(snap.ref);

        res(url);
      } catch (error) {
        rej(error);
      }
    });
  }

  private createFirstWorkspace(
    { user }: UserCredential,
    workspaceUID: string,
    userName: string,
    profilePicURL: string
  ): Promise<void> {
    const workspaceDoc = doc(this.workspaceCollection, workspaceUID);

    return setDoc(workspaceDoc, {
      uid: workspaceUID,
      name: userName,
      members: {
        [user.uid]: {
          name: userName,
          role: "owner",
          email: user.email,
          profilePic: profilePicURL,
        },
      },
      created: Date.now(),
    });
  }

  //update user info in Firestore
  private async setUserData(
    { user }: UserCredential,
    userName: string,
    profilePicURL: string,
    workspaceUID: string
  ) {
    const userDoc = doc(this.userCollection, user.uid);

    const data: User = {
      uid: user.uid,
      email: user.email,
      name: userName,
      profilePic: profilePicURL,
      currentWorkspaceUID: workspaceUID,
    };

    return setDoc(userDoc, data);
  }

  fetchDarkMode() {
    return this.store.select<User>("user").pipe(
      map((user) => {
        if (user) {
          localStorage.setItem("darkMode", `${user.darkMode}`); //add this
          return user.darkMode;
        }
        // check local storage
        else {
          const storageVal = localStorage.getItem("darkMode");
          return storageVal === "true";
        }
      })
    );
  }

  toggleDarkMode(val: boolean) {
    // set in store
    const user = this.store.getDirect<User>("user");
    user.darkMode = val;

    this.store.set("user", user);

    const userDoc = doc(this.userCollection, user.uid);

    // set in firestore
    updateDoc(userDoc, { darkMode: val });

    // set local storage
    localStorage.setItem("darkMode", `${val}`); //add this
  }
}
