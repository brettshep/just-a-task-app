import { Injectable, inject } from "@angular/core";

import {
  Firestore,
  CollectionReference,
  collection,
  doc,
  collectionData,
  writeBatch,
  orderBy,
  query,
  setDoc,
  updateDoc,
  DocumentReference,
  getDoc,
  deleteDoc,
  where,
} from "@angular/fire/firestore";
import { AuthService } from "./auth.service";
import { tap, finalize, switchMap, distinctUntilChanged } from "rxjs/operators";
import { Store } from "../store";
import {
  Workspace,
  Project,
  Card,
  Task,
  Member,
  User,
  CalendarDayDBList,
} from "../../../interfaces";
import { Observable, of, BehaviorSubject } from "rxjs";
import {
  FormProject,
  CalendarDay,
  CalendarCard,
  CalendarDayDB,
} from "../../../interfaces";
import { Router, ActivatedRoute } from "@angular/router";
import { CardUpdateType } from "../../../interfaces";

@Injectable({
  providedIn: "root",
})
export class ProjectService {
  userCollection: CollectionReference<User>;
  workspaceCollection: CollectionReference<Workspace>;

  constructor(
    private fireStore: Firestore = inject(Firestore),
    private authServ: AuthService = inject(AuthService),
    private store: Store,
    private router: Router
  ) {
    this.userCollection = collection(
      this.fireStore,
      "users"
    ) as CollectionReference<User>;
    this.workspaceCollection = collection(
      this.fireStore,
      "workspaces"
    ) as CollectionReference<Workspace>;
  }

  millisInDay = 1000 * 60 * 60 * 24;

  currWorkspace$: BehaviorSubject<Workspace> = new BehaviorSubject(null);
  currProject$: BehaviorSubject<Project> = new BehaviorSubject(null);
  currSelectedUser$: BehaviorSubject<string> = new BehaviorSubject(null);

  // prettier-ignore
  months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  //-----------GLOBAL SUBSCRIPTIONS------------

  /**
   * @remarks Fetches Workspaces and sets them to the store, set current workspace, based on user.currentWorkspaceUID.Only subscibed to once in AppComponent
   */
  appFetchWorkspaces(): Observable<Workspace[]> {
    const fsSub = this.authServ.user$
      .pipe(
        switchMap((user: User) => {
          if (!user) return of([]);
          else {
            const q = query(
              this.workspaceCollection,
              orderBy(`members.${user.uid}`)
            );

            return collectionData(q).pipe(
              tap((workspaces: Workspace[]) => {
                const sorted = workspaces.sort((a, b) => a.created - b.created);
                // set workspace list
                this.store.set("workspaceList", sorted);
                // set individual workspaces
                sorted.forEach((workspace) => {
                  this.store.set(`workspace_${workspace.uid}`, workspace);
                  // set currworkspace
                  if (workspace.uid === user.currentWorkspaceUID) {
                    this.currWorkspace$.next(workspace);
                  }
                });
              })
            );
          }
        })
      )
      .subscribe();

    return this.store.select<Workspace[]>("workspaceList").pipe(
      finalize(() => {
        fsSub.unsubscribe();
      })
    );
  }

  /**
   * @remarks Fetches Projects and sets them to the store, given the current workspace. Only subscibed to once in app component. If no projects in workspace, and not on "/home", redirect to "/home". If no current project, and projects available, set first as current. Updates the current project on its DB updates.
   */
  appFetchProjects(): Observable<Project[]> {
    return this.currWorkspace$.pipe(
      switchMap((workspace) => {
        if (workspace) {
          const projectCollection = collection(
            this.workspaceCollection,
            workspace.uid,
            "projects"
          ) as CollectionReference<Project>;

          const fsSub = collectionData(projectCollection)
            .pipe(
              tap((projects) => {
                // if not on home or invite and no projects, go home
                if (
                  this.router.url !== "/home" &&
                  !this.router.url.includes("invite") &&
                  !projects.length
                ) {
                  this.setCurrentProject(null);
                  this.router.navigateByUrl("/home");
                  console.log("go home");
                }

                if (this.router.url !== "/home" && this.currProject$.value) {
                  if (
                    !projects.find(
                      (proj) => proj.uid === this.currProject$.value.uid
                    )
                  ) {
                    console.log("currProj not in db", this.currProject$.value);
                    this.setCurrentProject(null);
                    this.router.navigateByUrl("/home");
                  }
                }

                const sorted = projects.sort((a, b) => a.created - b.created);
                // set workspace list
                this.store.set(`projectList_${workspace.uid}`, sorted);
                // set individual workspaces
                sorted.forEach((project) => {
                  // update currproject on DB updates
                  if (
                    this.currProject$.value &&
                    this.currProject$.value.uid === project.uid
                  ) {
                    this.setCurrentProject(project);
                  }
                });
                if (projects.length && !this.currProject$.value) {
                  this.setCurrentProject(projects[0]);
                }
              })
            )
            .subscribe();
          return this.store
            .select<Project[]>(`projectList_${workspace.uid}`)
            .pipe(
              finalize(() => {
                fsSub.unsubscribe();
              })
            );
        } else {
          return [];
        }
      })
    );
  }

  ///////////////////////////////////////////////////////////////////

  //-----------------WORKSPACE CRUD------------------

  /**
   * @remarks Fetches workspaceList, a Workspace[], from store
   */
  fetchWorkspaces(): Observable<Workspace[]> {
    return this.store.select("workspaceList");
  }

  /**
   * @remarks Fetches projectList, a Project[], from store
   */
  fetchProjects(): Observable<Project[]> {
    return this.currWorkspace$.pipe(
      switchMap((workspace: Workspace) => {
        if (workspace) {
          return this.store.select<Project[]>(`projectList_${workspace.uid}`);
        } else return of([]);
      })
    );
  }

  /**
   * @remarks Create new workspace in Firebase then update user currentWorkspaceUID, remove Current Project
   */
  async createWorkspace(name: string) {
    const user = this.store.getDirect<User>("user");
    const workspaceDoc = doc(this.workspaceCollection);
    const workspace: Workspace = {
      uid: workspaceDoc.id,
      name,
      members: {
        [user.uid]: {
          name: user.name,
          role: "owner",
          email: user.email,
          profilePic: user.profilePic,
        },
      },
      created: Date.now(),
    };

    // update currWorkspace$
    user.currentWorkspaceUID = workspaceDoc.id;

    this.currWorkspace$.next(workspace);

    // remove currProj$
    this.setCurrentProject(null);

    // set in fb
    await setDoc(workspaceDoc, workspace);

    await updateDoc(doc(this.userCollection, user.uid), {
      currentWorkspaceUID: workspaceDoc.id,
    });
  }

  /**
   * @remarks Select new workspace by updating user currentWorkspaceUID remove Current Project. If workspace is in store and has projects, go ahead and set first one to current, otherwise set to null
   */
  async selectWorkspace(workspace: Workspace) {
    // update currWorkspace$
    this.currWorkspace$.next(workspace);

    // if workspace has any projects in store, set first one as current
    const workspaceStoreProjects = this.store.getDirect<Project[]>(
      `projectList_${workspace.uid}`
    );
    if (workspaceStoreProjects && workspaceStoreProjects.length > 0) {
      this.setCurrentProject(workspaceStoreProjects[0]);
    } else {
      // remove currProj$
      this.setCurrentProject(null);
    }

    // update user workspace
    const user = this.store.getDirect<User>("user");

    await updateDoc(doc(this.userCollection, user.uid), {
      currentWorkspaceUID: workspace.uid,
    });
  }

  //-----------------PROJECT CRUD-------------------

  /**
   * @remarks Create new project,first in store, then Firebase. Set as Current Project
   * @returns Newly created project UID
   */
  createProject(e: FormProject): string {
    const projDoc = doc(
      collection(
        this.workspaceCollection,
        this.currWorkspace$.value.uid,
        "projects"
      )
    ) as unknown as DocumentReference<Project>;

    const categories = e.categories.map((cat) => {
      cat.uid = doc(this.workspaceCollection).id;
      return cat;
    });

    const project: Project = {
      uid: projDoc.id,
      name: e.name,
      bgColor: e.bgColor,
      fontColor: e.fontColor,
      totalTask: 0,
      ownerUID: this.UID,
      categories,
      created: Date.now(),
    };

    // set as current project
    this.setCurrentProject(project);

    // set in fb
    setDoc(projDoc, project);

    return projDoc.id;
  }

  /**
   * @remarks Update current project,first in store, then Firebase.
   */
  async updateProject(_project: Project) {
    let project = { ..._project };

    // give categories ids if they dont have one
    project.categories = project.categories.map((cat) => {
      cat.uid = cat.uid ? cat.uid : doc(this.workspaceCollection).id;
      return cat;
    });

    // set as current project
    this.setCurrentProject(project);

    // set in fb
    const projDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "projects",
      project.uid
    ) as unknown as DocumentReference<Project>;

    await updateDoc(projDoc, project);
  }

  /**
   * @remarks Delete current project and all of its cards and scheduled cal cards,first in store, then Firebase.
   */
  async deleteProject(project: Project) {
    // remove project from store
    const storeProjects = this.store.getDirect<Project[]>(
      `projectList_${this.currWorkspace$.value.uid}`
    );

    const storeProjIndex = storeProjects.findIndex(
      (_proj) => _proj.uid === project.uid
    );

    this.store.set(
      `projectList_${this.currWorkspace$.value.uid}`,
      storeProjects
    );

    storeProjects.splice(storeProjIndex, 1);

    // go to first project, or home if none
    if (storeProjects.length > 0) {
      this.setCurrentProject(storeProjects[0]);
    } else {
      this.setCurrentProject(null);
      this.router.navigateByUrl("/home");
    }

    // ---db operations---
    const batch = writeBatch(this.fireStore);

    // delete project cards
    const storeCards = this.store.getDirect<Card[]>(`cards_${project.uid}`);

    let tasksToUpdate: Task[] = [];

    storeCards.forEach((card) => {
      // set calCard to delete
      tasksToUpdate = [
        ...tasksToUpdate,
        ...card.tasks.filter((task) => task.scheduledDayUID),
      ];

      // set card ref
      const cardDoc = doc(
        this.workspaceCollection,
        this.currWorkspace$.value.uid,
        "projects",
        project.uid,
        "cards",
        card.uid
      ) as unknown as DocumentReference<Card>;

      batch.delete(cardDoc);
    });

    // delete project in fb
    const projectDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "projects",
      project.uid
    ) as unknown as DocumentReference<Project>;

    batch.delete(projectDoc);

    // delete cal cards
    const taskPromises: Promise<null>[] = [];

    if (tasksToUpdate.length) {
      // group task to update by day
      const taskUIDsGroupedByDays: Record<string, Set<string>> = {};
      for (const task of tasksToUpdate) {
        if (!taskUIDsGroupedByDays[task.scheduledDayUID]) {
          taskUIDsGroupedByDays[task.scheduledDayUID] = new Set<string>();
        }
        taskUIDsGroupedByDays[task.scheduledDayUID].add(task.uid);
      }

      for (const dayUID in taskUIDsGroupedByDays) {
        const updatePromise = new Promise<null>(async (resolve, reject) => {
          const taskSet = taskUIDsGroupedByDays[dayUID];

          const daysDoc = doc(
            this.workspaceCollection,
            this.currWorkspace$.value.uid,
            "days",
            dayUID
          ) as unknown as DocumentReference<CalendarDayDB>;

          const daySnap = await getDoc(daysDoc);
          const day = daySnap.data() as CalendarDayDB;

          // delete all task that are in category
          day.calendarCards = day.calendarCards.filter(
            (calCard) => !taskSet.has(calCard.task.uid)
          );

          // update day
          batch.update(daysDoc, { calendarCards: day.calendarCards });
          resolve(null);
        });

        //add to promise array
        taskPromises.push(updatePromise);
      }
    }

    // wait for all promises to resolve
    await Promise.all(taskPromises);

    await batch.commit();
  }

  /**
   * @remarks Sets the current project
   */
  setCurrentProject(e: Project) {
    this.currProject$.next(e);
  }

  /**
   * @remarks Delete category from project and either delete its cards, or move them somewhere else based on categoryUID ,first in store, then Firebase.
   */
  async deleteProjectCategory(
    categoryToDelete: string,
    categoryToMoveTo: string,
    project: Project
  ) {
    // batch for card operations
    const batch = writeBatch(this.fireStore);

    // current store cards
    let projectCards = this.store.getDirect<Card[]>(`cards_${project.uid}`);

    let tasksToUpdate: Task[] = [];

    for (let i = 0; i < projectCards.length; i++) {
      const card = projectCards[i];
      if (card.categoryID === categoryToDelete) {
        //get all tasks to update that are scheduled
        tasksToUpdate = [
          ...tasksToUpdate,
          ...card.tasks.filter((task) => task.scheduledDayUID),
        ];

        const cardDoc = doc(
          this.workspaceCollection,
          this.currWorkspace$.value.uid,
          "projects",
          project.uid,
          "cards",
          card.uid
        ) as unknown as DocumentReference<Card>;

        // move cards to category
        if (categoryToMoveTo) {
          card.categoryID = categoryToMoveTo === "All" ? "" : categoryToMoveTo;
          batch.update(cardDoc, { categoryID: card.categoryID });
        }
        // delete cards
        else {
          projectCards[i] = null;
          batch.delete(cardDoc);
        }
      }
    }

    // check to see if need to update cal cards
    const taskPromises: Promise<null>[] = [];

    if (tasksToUpdate.length) {
      // group task to update by day
      const taskUIDsGroupedByDays: Record<string, Set<string>> = {};
      for (const task of tasksToUpdate) {
        if (!taskUIDsGroupedByDays[task.scheduledDayUID]) {
          taskUIDsGroupedByDays[task.scheduledDayUID] = new Set<string>();
        }
        taskUIDsGroupedByDays[task.scheduledDayUID].add(task.uid);
      }

      //loop through days
      for (const dayUID in taskUIDsGroupedByDays) {
        const updatePromise = new Promise<null>(async (resolve, reject) => {
          const taskSet = taskUIDsGroupedByDays[dayUID];

          const daysDoc = doc(
            this.workspaceCollection,
            this.currWorkspace$.value.uid,
            "days",
            dayUID
          ) as unknown as DocumentReference<CalendarDayDB>;

          const daySnap = await getDoc(daysDoc);
          const day = daySnap.data() as CalendarDayDB;

          if (categoryToMoveTo) {
            //if moving to category, update all tasks categories
            for (const calCard of day.calendarCards) {
              if (taskSet.has(calCard.task.uid)) {
                calCard.categoryID =
                  categoryToMoveTo === "All" ? "" : categoryToMoveTo;
              }
            }
          } else {
            // delete all task that are in category
            day.calendarCards = day.calendarCards.filter(
              (calCard) => !taskSet.has(calCard.task.uid)
            );
          }

          // update day
          batch.update(daysDoc, { calendarCards: day.calendarCards });
          resolve(null);
        });

        //add to promise array
        taskPromises.push(updatePromise);
      }
    }

    // wait for all promises to resolve
    await Promise.all(taskPromises);

    // batch card update/delete
    await batch.commit();

    //filter out null cards
    projectCards = projectCards.filter((card) => !!card);

    // set cards in store
    this.store.set(`cards_${project.uid}`, projectCards);

    // delete category from project
    project.categories = project.categories.filter(
      (cat) => cat.uid !== categoryToDelete
    );

    await this.updateProject(project);
  }

  //------------------CARDS CRUD--------------------
  /**
   * @remarks Fetches cards and sets them to the store, given the current project.
   */
  fetchCards(): Observable<Card[]> {
    return this.currProject$.pipe(
      distinctUntilChanged((a, b) => {
        if (a && b && a.uid === b.uid) return true;
        return false;
      }),
      switchMap((project) => {
        if (project) {
          const cardCollection = collection(
            this.workspaceCollection,
            this.currWorkspace$.value.uid,
            "projects",
            project.uid,
            "cards"
          ) as CollectionReference<Card>;

          const fsSub = collectionData(cardCollection)
            .pipe(
              tap((cards) => {
                // console.log("getting cards for: ", project.uid);
                // set cards
                this.store.set(`cards_${project.uid}`, cards);
              })
            )
            .subscribe();
          return this.store.select<Project[]>(`cards_${project.uid}`).pipe(
            finalize(() => {
              // console.log("card unsub");
              fsSub.unsubscribe();
            })
          );
        } else {
          return [];
        }
      })
    );
  }

  /**
   * @remarks Create new card ,first in store, then Firebase.
   */
  createCard(project: Project, category: string) {
    const cardDoc = doc(
      collection(
        this.workspaceCollection,
        this.currWorkspace$.value.uid,
        "projects",
        project.uid,
        "cards"
      )
    ) as unknown as DocumentReference<Card>;

    const card: Card = {
      uid: cardDoc.id,
      title: "",
      tasks: [],
      categoryID: category,
      created: Date.now(),
      order: 1000,
    };

    // set in store
    let cards = this.store.getDirect<Card[]>(`cards_${project.uid}`);
    cards = [...cards, card];
    this.store.set(`cards_${project.uid}`, cards);

    // set in fb

    setDoc(cardDoc, card);
  }

  duplicateCard(card: Card, project: Project) {
    const cardDoc = doc(
      collection(
        this.workspaceCollection,
        this.currWorkspace$.value.uid,
        "projects",
        project.uid,
        "cards"
      )
    ) as unknown as DocumentReference<Card>;

    const newCard: Card = {
      ...card,
      uid: cardDoc.id,
      created: Date.now(),
      order: (card.order ?? 0) + 0.5,
    };

    //give new IDs to tasks
    newCard.tasks = newCard.tasks.map((task) => {
      return {
        name: task.name,
        uid: doc(this.workspaceCollection).id,
        completed: false,
        assignedUserUID: task.assignedUserUID || null,
      };
    });

    // set in store
    let cards = this.store.getDirect<Card[]>(`cards_${project.uid}`);
    cards = [...cards, newCard];
    this.store.set(`cards_${project.uid}`, cards);

    // set in fb
    setDoc(cardDoc, newCard);
  }

  /**
   * @remarks Update card ,first in store, then Firebase.
   */
  updateCard(
    card: Card,
    project: Project,
    updateType: CardUpdateType,
    task: Task
  ) {
    // set update key
    let keyToUpdate: string;
    switch (updateType) {
      case "tasksCreate":
        keyToUpdate = "tasks";
        break;
      case "tasksEdit":
        keyToUpdate = "tasks";
        break;
      case "tasksDelete":
        keyToUpdate = "tasks";
        break;
      case "tasksOrder":
        keyToUpdate = "tasks";
        break;
      case "tasksComplete":
        keyToUpdate = "tasks";
        break;
      case "assignedUserUID":
        keyToUpdate = "tasks";
        break;
      case "cardAssignUser":
        keyToUpdate = "tasks";
        break;
      default:
        keyToUpdate = updateType;
        break;
    }

    // set in store
    let cards = this.store.getDirect<Card[]>(`cards_${project.uid}`);
    const index = cards.findIndex((_card) => _card.uid === card.uid);
    cards.splice(index, 1, card);
    this.store.set(`cards_${project.uid}`, cards);

    // set in fb
    const cardDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "projects",
      project.uid,
      "cards",
      card.uid
    ) as unknown as DocumentReference<Card>;

    updateDoc(cardDoc, { [keyToUpdate]: card[keyToUpdate] });

    //check to see if need to update cal cards
    //run through task and see if cards

    switch (updateType) {
      case "tasksEdit":
        this.queryCalCardTaskEdit(task);
        break;
      case "tasksDelete":
        this.queryCalCardTaskEdit(task, true);
        break;
      case "assignedUserUID":
        this.queryCalCardTaskEdit(task);
        break;
      case "tasksComplete":
        this.queryCalCardTaskEdit(task);
        break;
      case "title":
        this.queryCalCardEdit(card, keyToUpdate);
        break;
      case "categoryID":
        this.queryCalCardEdit(card, keyToUpdate);
        break;
      case "cardAssignUser":
        this.queryCalCardEdit(card, "allTasksAssignedUsers");
        break;
      default:
        break;
    }
  }

  updateCardsOrder(project: Project, cards: Card[]) {
    const batch = writeBatch(this.fireStore);

    for (let i = 0; i < cards.length; i++) {
      const card = cards[i];
      const cardDoc = doc(
        this.workspaceCollection,
        this.currWorkspace$.value.uid,
        "projects",
        project.uid,
        "cards",
        card.uid
      ) as unknown as DocumentReference<Card>;

      batch.update(cardDoc, { order: i });
    }

    batch.commit();
  }

  /**
   * @remarks Delete card ,first in store, then Firebase.
   */
  async deleteCard(card: Card, project: Project) {
    // set in store
    let cards = this.store.getDirect<Card[]>(`cards_${project.uid}`);
    const index = cards.findIndex((_card) => _card.uid === card.uid);
    cards.splice(index, 1);
    this.store.set(`cards_${project.uid}`, cards);

    // set in fb
    const cardDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "projects",
      project.uid,
      "cards",
      card.uid
    ) as unknown as DocumentReference<Card>;

    deleteDoc(cardDoc);

    //delete task from calCards if abailable
    const tasksToUpdate = card.tasks.filter((task) => task.scheduledDayUID);
    if (tasksToUpdate.length) {
      // group task to update by days
      const taskGroupedByDays = {};
      tasksToUpdate.forEach((task) => {
        if (!taskGroupedByDays[task.scheduledDayUID]) {
          taskGroupedByDays[task.scheduledDayUID] = [];
        }
        taskGroupedByDays[task.scheduledDayUID].push(task);
      });

      for (const dayUID in taskGroupedByDays) {
        if (taskGroupedByDays.hasOwnProperty(dayUID)) {
          const taskArr: Task[] = taskGroupedByDays[dayUID];

          const daysDoc = doc(
            this.workspaceCollection,
            this.currWorkspace$.value.uid,
            "days",
            dayUID
          ) as unknown as DocumentReference<CalendarDayDB>;

          getDoc(daysDoc).then((daySnap) => {
            const day = daySnap.data();

            // delete all task
            taskArr.forEach((task) => {
              const cardIndex = day.calendarCards.findIndex(
                (calCard) => calCard.task.uid === task.uid
              );
              day.calendarCards.splice(cardIndex, 1);
            });

            // update day
            updateDoc(daysDoc, { calendarCards: day.calendarCards });
          });
        }
      }
    }
  }

  //--------------CALENDAR DAY CRUD-----------------

  //--------query operations for calCards-------
  private async queryCalCardTaskEdit(task: Task, deleteTask?: boolean) {
    if (!task.scheduledDayUID) return;
    // update db

    const daysDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "days",
      task.scheduledDayUID.toString()
    ) as unknown as DocumentReference<CalendarDayDB>;

    const daysSnap = await getDoc(daysDoc);

    const day = daysSnap.data() as CalendarDayDB;

    // update task
    if (!deleteTask) {
      const cardIndex = day.calendarCards.findIndex(
        (card) => card.task.uid === task.uid
      );
      day.calendarCards[cardIndex].task = task;
    }

    // delete task
    else {
      day.calendarCards = day.calendarCards.filter(
        (card) => card.task.uid !== task.uid
      );
    }

    updateDoc(daysDoc, { calendarCards: day.calendarCards });
  }

  private queryCalCardEdit(card: Card, keyToUpdate: string) {
    const tasksToUpdate = card.tasks.filter((task) => task.scheduledDayUID);

    const taskGroupedByDays = {};
    tasksToUpdate.forEach((task) => {
      if (!taskGroupedByDays[task.scheduledDayUID]) {
        taskGroupedByDays[task.scheduledDayUID] = [];
      }
      taskGroupedByDays[task.scheduledDayUID].push(task);
    });

    for (const dayUID in taskGroupedByDays) {
      if (taskGroupedByDays.hasOwnProperty(dayUID)) {
        const taskArr: Task[] = taskGroupedByDays[dayUID];

        const daysDoc = doc(
          this.workspaceCollection,
          this.currWorkspace$.value.uid,
          "days",
          dayUID
        ) as unknown as DocumentReference<CalendarDayDB>;

        getDoc(daysDoc).then((snap) => {
          const day = snap.data() as CalendarDayDB;
          // update all task
          taskArr.forEach((task) => {
            const cardIndex = day.calendarCards.findIndex(
              (calCard) => calCard.task.uid === task.uid
            );

            if (keyToUpdate === "allTasksAssignedUsers") {
              day.calendarCards[cardIndex].task.assignedUserUID =
                card.tasks[0].assignedUserUID;
            } else {
              const calKeyToUpdate =
                keyToUpdate === "title" ? "cardName" : keyToUpdate;
              day.calendarCards[cardIndex][calKeyToUpdate] = card[keyToUpdate];
            }
          });

          // update day
          updateDoc(daysDoc, { calendarCards: day.calendarCards });
        });
      }
    }
  }

  /**
   * @remarks Fetches CalendarDays + and - range of currDateUID and sets them to the store, given the current currDateUID.
   */
  fetchCalendarDays(
    currDate: BehaviorSubject<Date>,
    range: number
  ): Observable<CalendarDay[]> {
    return currDate.pipe(
      switchMap((date: Date) => {
        if (date) {
          const dateID = date.getTime();

          // start date
          const startDate = new Date(date.getTime());
          startDate.setDate(startDate.getDate() - range);
          // end date
          const endDate = new Date(date.getTime());
          endDate.setDate(endDate.getDate() + range);

          const startAt = startDate.getTime();
          const endAt = endDate.getTime();

          const daysCollection = collection(
            this.workspaceCollection,
            this.currWorkspace$.value.uid,
            "days"
          ) as CollectionReference<CalendarDayDB>;

          const q = query(
            daysCollection,
            where("uid", ">=", startAt),
            where("uid", "<=", endAt),
            orderBy("uid")
          );

          const fsSub = collectionData(q)
            .pipe(
              tap((days) => {
                let dbDaysList: CalendarDayDBList = {};
                days.forEach((day) => {
                  dbDaysList[day.uid] = day;
                });
                // set cards
                const filledDays: CalendarDay[] = this.fillDays(
                  startAt,
                  endAt,
                  dbDaysList
                );

                this.store.set(`days_${dateID}`, filledDays);
              })
            )
            .subscribe();

          return this.store.select<CalendarDay[]>(`days_${dateID}`).pipe(
            finalize(() => {
              // console.log("days unsub");
              fsSub.unsubscribe();
            })
          );
        } else {
          return [];
        }
      })
    );
  }

  /**
   * @remarks Given start and end date ID's, and dbCalendarDays, contructs a compelte list of CalendearDays for entire range, with calendarCards added when available
   */
  private fillDays(
    startAt: number,
    endAt: number,
    dbDaysList: CalendarDayDBList
  ): CalendarDay[] {
    let calDays = [];

    for (let i = startAt; i <= endAt; i += this.millisInDay) {
      const date = new Date(i);
      const calendarDay: CalendarDay = {
        dayNumber: date.getDay(),
        dateNumber: date.getDate(),
        monthNumber: date.getMonth(),
        yearNumber: date.getFullYear(),
        uid: i,
        calendarCards: dbDaysList[i] ? dbDaysList[i].calendarCards : [],
      };
      calDays.push(calendarDay);
    }

    return calDays;
  }

  /**
   * @remarks Updates task to be scheduled to given date, or removed schedule. Also update card in which the task was assigned. Batch operations so they are simultaneous
   */
  toggleScheduleTask(
    task: Task,
    card: Card,
    project: Project,
    dateToSet: number | null,
    dayName: string,
    currentDateUID: number,
    rescheduling: boolean
  ) {
    let storeCalDays = this.store.getDirect<CalendarDay[]>(
      `days_${currentDateUID}`
    );

    // create batch so updates can be set in one go
    const batch = writeBatch(this.fireStore);

    //----Create new calendar card and update calendar day
    if (dateToSet && dayName) {
      //delete previous card and update to new day
      if (rescheduling) {
        const calDayToEditIndex = storeCalDays.findIndex(
          (day) => day.uid === task.scheduledDayUID
        );
        // see if in store, otherwise have to query
        if (calDayToEditIndex !== -1) {
          storeCalDays[calDayToEditIndex].calendarCards = storeCalDays[
            calDayToEditIndex
          ].calendarCards.filter((card) => card.task.uid !== task.uid);

          const daysDoc = doc(
            this.workspaceCollection,
            this.currWorkspace$.value.uid,
            "days",
            task.scheduledDayUID.toString()
          ) as unknown as DocumentReference<CalendarDayDB>;

          batch.set(daysDoc, {
            uid: storeCalDays[calDayToEditIndex].uid,
            calendarCards: storeCalDays[calDayToEditIndex].calendarCards,
          });
        }
        // query day then update
        else {
          const daysDoc = doc(
            this.workspaceCollection,
            this.currWorkspace$.value.uid,
            "days",
            task.scheduledDayUID.toString()
          ) as unknown as DocumentReference<CalendarDayDB>;

          getDoc(daysDoc).then((snap) => {
            const day: CalendarDayDB = snap.data() as any;

            day.calendarCards = day.calendarCards.filter(
              (card) => card.task.uid !== task.uid
            );

            batch.update(daysDoc, {
              calendarCards: day.calendarCards,
            });
          });
        }
      }

      // create updated calcard fir newly selected day
      const calDayToEditIndex = storeCalDays.findIndex(
        (card) => card.uid === dateToSet
      );

      // insert task
      const newCalCard: CalendarCard = {
        projectUID: project.uid,
        cardUID: card.uid,
        task,
        cardName: card.title,
        categoryID: card.categoryID,
      };

      storeCalDays[calDayToEditIndex].calendarCards.push(newCalCard);

      // update db
      const daysDoc = doc(
        this.workspaceCollection,
        this.currWorkspace$.value.uid,
        "days",
        dateToSet.toString()
      ) as unknown as DocumentReference<CalendarDayDB>;

      batch.set(daysDoc, {
        calendarCards: storeCalDays[calDayToEditIndex].calendarCards,
        uid: dateToSet,
      });

      //edit task
      task.scheduledDayUID = dateToSet;
      task.scheduledDayName = dayName;
    }
    //---Delete calendar card and update calendar day
    else {
      const dayToDeleteData = this.deleteCalendarCard(
        task.scheduledDayUID,
        storeCalDays,
        task.uid
      );

      // update db
      const daysDoc = doc(
        this.workspaceCollection,
        this.currWorkspace$.value.uid,
        "days",
        task.scheduledDayUID.toString()
      ) as unknown as DocumentReference<CalendarDayDB>;

      batch.set(daysDoc, dayToDeleteData.dbDay);
      storeCalDays = dayToDeleteData.calDays;

      //edit task
      delete task.scheduledDayUID;
      delete task.scheduledDayName;
    }

    // update store
    this.store.set(`days_${currentDateUID}`, storeCalDays);

    // batch commit
    batch.commit();

    // update card tasks, use "tasksCreate" so it doesnt update calCards again
    this.updateCard(card, project, "tasksCreate", null);
  }

  /**
   * @remarks Updates the calendarDay calCard[] order, first in store, then DB
   */
  updateCalendCardOrder(
    currentDateUID: number,
    dayIndex: number,
    cards: CalendarCard[]
  ) {
    let storeCalDays = this.store.getDirect<CalendarDay[]>(
      `days_${currentDateUID}`
    );

    storeCalDays[dayIndex].calendarCards = cards;

    // update store
    this.store.set(`days_${currentDateUID}`, storeCalDays);

    // update db
    const daysDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "days",
      storeCalDays[dayIndex].uid.toString()
    ) as unknown as DocumentReference<CalendarDayDB>;

    updateDoc(daysDoc, { calendarCards: cards });
  }

  /**
   * @remarks Activated when card is dragged to new day. Updates the previous calendarDay calCard[],and the new one. Also update the card in which it belonged to, querying it and updating if not in current project
   */
  updateCalCardNewDayDrag(
    currentDateUID: number,
    calCardsPrev: CalendarCard[],
    calCards: CalendarCard[],
    calCardIndex: number,
    dayIndexPrev: number,
    dayIndex: number
  ) {
    const batch = writeBatch(this.fireStore);
    const calCardUpdating = calCards[calCardIndex];
    const projectUID = calCardUpdating.projectUID;

    // get data from store to update
    const storeProjects = this.store.getDirect<Project[]>(
      `projectList_${this.currWorkspace$.value.uid}`
    );

    const storeProj = storeProjects.find((_proj) => _proj.uid === projectUID);

    let storeCalDays = this.store.getDirect<CalendarDay[]>(
      `days_${currentDateUID}`
    );

    const newDayUID: number = storeCalDays[dayIndex].uid;

    // edit card from current project
    if (storeProj.uid === this.currProject$.value.uid) {
      let cards = this.store.getDirect<Card[]>(`cards_${storeProj.uid}`);
      const cardIndex = cards.findIndex(
        (_card) => _card.uid === calCardUpdating.cardUID
      );

      const taskIndex = cards[cardIndex].tasks.findIndex(
        (task) => task.uid === calCardUpdating.task.uid
      );

      const setDate = new Date(newDayUID);
      const monthName = this.months[setDate.getMonth()];
      const date = setDate.getDate();
      const dayName = `${monthName} ${date}`;

      // update card task scheduled day
      cards[cardIndex].tasks[taskIndex].scheduledDayUID = newDayUID;

      cards[cardIndex].tasks[taskIndex].scheduledDayName = dayName;

      this.store.set(`cards_${storeProj.uid}`, cards);

      // update card
      const cardDoc = doc(
        this.workspaceCollection,
        this.currWorkspace$.value.uid,
        "projects",
        storeProj.uid,
        "cards",
        cards[cardIndex].uid
      ) as unknown as DocumentReference<Card>;

      batch.update(cardDoc, { tasks: cards[cardIndex].tasks });
    }
    // query card and update it
    else {
      this.queryToUpdateCardTask(calCardUpdating, "update", newDayUID);
    }

    // update prev day calcards
    storeCalDays[dayIndexPrev].calendarCards = calCardsPrev;

    // update new day calccards
    storeCalDays[dayIndex].calendarCards = calCards;

    // update store
    this.store.set(`days_${currentDateUID}`, storeCalDays);

    // update prev day
    const prevDaysDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "days",
      storeCalDays[dayIndexPrev].uid.toString()
    ) as unknown as DocumentReference<CalendarDayDB>;

    batch.set(prevDaysDoc, {
      calendarCards: calCardsPrev,
      uid: storeCalDays[dayIndexPrev].uid,
    });

    // update new day
    const newDaysDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "days",
      storeCalDays[dayIndex].uid.toString()
    ) as unknown as DocumentReference<CalendarDayDB>;

    batch.set(newDaysDoc, {
      calendarCards: calCards,
      uid: storeCalDays[dayIndex].uid,
    });

    // commit
    batch.commit();
  }

  /**
   * @remarks Checks the calendarCard from calDay. first in store, then DB. Also update the card in which it belonged to, querying it and updating if not in current project
   */
  checkCalendarCard(
    currentDateUID: number,
    calCard: CalendarCard,
    cardIndex: number,
    dayIndex: number
  ) {
    const toggleState = !calCard.task.completed;

    calCard.task.completed = toggleState;

    const batch = writeBatch(this.fireStore);

    const projectUID = calCard.projectUID;

    // get data from store to update
    const storeProjects = this.store.getDirect<Project[]>(
      `projectList_${this.currWorkspace$.value.uid}`
    );

    const storeProj = storeProjects.find((_proj) => _proj.uid === projectUID);

    let storeCalDays = this.store.getDirect<CalendarDay[]>(
      `days_${currentDateUID}`
    );

    //-------update card--------
    // edit card from current project
    if (storeProj.uid === this.currProject$.value.uid) {
      let cards = this.store.getDirect<Card[]>(`cards_${storeProj.uid}`);
      const cardIndex = cards.findIndex(
        (_card) => _card.uid === calCard.cardUID
      );
      const taskIndex = cards[cardIndex].tasks.findIndex(
        (task) => task.uid === calCard.task.uid
      );

      // toggle card task
      cards[cardIndex].tasks[taskIndex].completed = toggleState;

      this.store.set(`cards_${storeProj.uid}`, cards);

      // update card in db
      const cardDoc = doc(
        this.workspaceCollection,
        this.currWorkspace$.value.uid,
        "projects",
        storeProj.uid,
        "cards",
        cards[cardIndex].uid
      ) as unknown as DocumentReference<Card>;

      batch.update(cardDoc, { tasks: cards[cardIndex].tasks });
    }
    // query card from other project and update
    else {
      this.queryToUpdateCardTask(calCard, "update");
    }

    // ------Update CalDay-----

    // update cal card from day
    storeCalDays[dayIndex].calendarCards[cardIndex].task.completed =
      toggleState;

    // update store
    this.store.set(`days_${currentDateUID}`, storeCalDays);

    const daysDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "days",
      storeCalDays[dayIndex].uid.toString()
    ) as unknown as DocumentReference<CalendarDayDB>;

    batch.set(daysDoc, {
      calendarCards: storeCalDays[dayIndex].calendarCards,
      uid: storeCalDays[dayIndex].uid,
    });

    // ---commit db---
    batch.commit();
  }

  /**
   * @remarks Given calendar card and newDayUID, query card that isnt part of current project and update its tasks to reflect the newly scheduled card
   */
  private async queryToUpdateCardTask(
    calCard: CalendarCard,
    action: "delete" | "update",
    newDayUID?: number
  ) {
    const cardDoc = doc(
      this.workspaceCollection,
      this.currWorkspace$.value.uid,
      "projects",
      calCard.projectUID,
      "cards",
      calCard.cardUID
    ) as unknown as DocumentReference<Card>;

    const cardSnap = await getDoc(cardDoc);

    const card = cardSnap.data();

    const taskIndex = card.tasks.findIndex(
      (task) => task.uid === calCard.task.uid
    );

    if (action === "update" && newDayUID) {
      const setDate = new Date(newDayUID);
      const monthName = this.months[setDate.getMonth()];
      const date = setDate.getDate();
      const dayName = `${monthName} ${date}`;

      // update card task scheduled day
      card.tasks[taskIndex].scheduledDayUID = newDayUID;

      card.tasks[taskIndex].scheduledDayName = dayName;
    } else {
      //just update task
      card.tasks[taskIndex] = calCard.task;
    }

    // set in fb
    updateDoc(cardDoc, { tasks: card.tasks });
  }

  /**
   * @remarks Given dayUID,calendarDay[], and taskUID, remove the calendarCard from the day and return the days[] which is directly mutated
   */
  private deleteCalendarCard(
    dayUID: number,
    calDays: CalendarDay[],
    taskUID: string
  ): { calDays: CalendarDay[]; dbDay: CalendarDayDB } {
    // edit calCard
    const calDayToEditIndex = calDays.findIndex((day) => day.uid === dayUID);

    calDays[calDayToEditIndex].calendarCards = calDays[
      calDayToEditIndex
    ].calendarCards.filter((card) => card.task.uid !== taskUID);

    return {
      calDays,
      dbDay: {
        calendarCards: calDays[calDayToEditIndex].calendarCards,
        uid: calDays[calDayToEditIndex].uid,
      },
    };
  }

  get UID() {
    return this.authServ.UID;
  }
}
