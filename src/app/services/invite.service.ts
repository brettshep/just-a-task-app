import { Injectable, inject } from "@angular/core";
import {
  CollectionReference,
  Firestore,
  collection,
  doc,
  setDoc,
  getDoc,
  deleteDoc,
  updateDoc,
} from "@angular/fire/firestore";
import { Functions, httpsCallable } from "@angular/fire/functions";
import {
  Workspace,
  Member,
  InviteEmailData,
  InviteDBData,
  User,
} from "../../../interfaces";
import { AuthService } from "./auth.service";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class InviteService {
  userCollection: CollectionReference<User>;
  inviteCollection: CollectionReference<InviteDBData>;
  workspaceCollection: CollectionReference<Workspace>;

  constructor(
    private authServ: AuthService = inject(AuthService),
    private fireStore: Firestore = inject(Firestore),
    private fireFunctions: Functions = inject(Functions)
  ) {
    this.userCollection = collection(
      this.fireStore,
      "users"
    ) as CollectionReference<User>;
    this.inviteCollection = collection(
      this.fireStore,
      "invites"
    ) as CollectionReference<InviteDBData>;
    this.workspaceCollection = collection(
      this.fireStore,
      "workspaces"
    ) as CollectionReference<Workspace>;
  }

  workspaceToAdd: string;

  async invite(to: string, workspace: Workspace) {
    const memberUID = this.authServ.UID;
    const fromMember = workspace.members[memberUID];

    const inviteDoc = doc(this.inviteCollection);

    await setDoc(inviteDoc, {
      workspaceUID: workspace.uid,
      workspaceName: workspace.name,
      from: fromMember.name,
    });

    const callable = httpsCallable<InviteEmailData>(
      this.fireFunctions,
      "sendEmail"
    );

    await callable({
      from: fromMember.name,
      to,
      workspace: workspace.name,
      inviteUID: inviteDoc.id,
    });
  }

  async getInvite(inviteUID: string): Promise<Partial<InviteDBData>> {
    const inviteDoc = doc(this.inviteCollection, inviteUID);
    const inviteSnap = await getDoc(inviteDoc);
    const inviteData = inviteSnap.data() as InviteDBData;
    return inviteData ?? { deleted: true };
  }

  acceptInvite(inviteUID: string, inviteData: InviteDBData): Promise<void> {
    return new Promise((res, rej) => {
      // add user to workspace
      const userSub = this.authServ.user$.subscribe(async (user) => {
        if (user) {
          const workspaceDoc = doc(
            this.workspaceCollection,
            inviteData.workspaceUID
          );

          const workspaceSnap = await getDoc(workspaceDoc);

          const workspace = workspaceSnap.data() as Workspace;

          workspace.members[user.uid] = {
            role: "member",
            profilePic: user.profilePic,
            name: user.name,
            email: user.email,
          };

          await updateDoc(workspaceDoc, { members: workspace.members });

          // set as current workspace
          await updateDoc(doc(this.userCollection, user.uid), {
            currentWorkspaceUID: workspace.uid,
          });

          res();

          // delete invite
          this.deleteInvite(inviteUID);

          // unsub from user
          userSub.unsubscribe();
        }
      });
    });
  }
  deleteInvite(inviteUID: string) {
    const docRef = doc(this.fireStore, "invites", inviteUID);
    deleteDoc(docRef);
  }
}
