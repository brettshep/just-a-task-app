import { Component, OnInit } from "@angular/core";
import { InviteDBData } from "../../../interfaces";
import { InviteService } from "../services/invite.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";

@Component({
  selector: "app-invite",
  template: `
    <div class="container">
      <div
        class="card"
        *ngIf="invite; else loading"
        [class.deleted]="invite.deleted"
      >
        <section>
          <div class="title">
            <h1 class="cardName" *ngIf="!invite.deleted; else deletedTitle">
              Join "{{ invite.workspaceName }}"
            </h1>

            <ng-template #deletedTitle>
              <h1 class="cardName">Invite Used or Nonexitent</h1>
            </ng-template>
          </div>

          <div class="acceptButtons" *ngIf="!invite.deleted; else goHomeButton">
            <button (click)="denyInvite()" type="button">Deny</button>
            <button (click)="acceptInvite(invite)" type="button" class="accent">
              Accept
            </button>
          </div>
          <ng-template #goHomeButton>
            <div class="acceptButtons">
              <button (click)="goHome()" type="button">Go Home</button>
            </div>
          </ng-template>
        </section>
      </div>
      <div *ngIf="loadingWorkspace" class="spinner"></div>
    </div>
    <ng-template #loading>
      <div class="spinner"></div>
    </ng-template>
  `,
  styleUrls: ["./invite.component.sass"],
})
export class InviteComponent implements OnInit {
  constructor(
    private inviteServ: InviteService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  invite: Partial<InviteDBData>;
  inviteUID: string;
  loadingWorkspace: boolean;

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(async (params) => {
      this.inviteUID = params["id"];
      this.invite = await this.inviteServ.getInvite(this.inviteUID);
    });
  }

  goHome() {
    this.router.navigateByUrl("/home");
  }

  async acceptInvite(inviteData: Partial<InviteDBData>) {
    if (this.loadingWorkspace) return;
    this.loadingWorkspace = true;
    await this.inviteServ.acceptInvite(
      this.inviteUID,
      inviteData as InviteDBData
    );
    this.goHome();
  }

  denyInvite() {
    if (this.loadingWorkspace) return;
    this.inviteServ.deleteInvite(this.inviteUID);
    this.goHome();
  }
}
