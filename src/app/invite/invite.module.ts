import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { InviteComponent } from "./invite.component";
import { RouterModule, Routes } from "@angular/router";

export const ROUTES: Routes = [
  {
    path: "",
    component: InviteComponent,
  },
  { path: "**", redirectTo: "/home" },
];

@NgModule({
  declarations: [InviteComponent],
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
})
export class InviteModule {}
