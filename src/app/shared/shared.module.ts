import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TextAreaDirective } from "./textarea.directive";
import { ClickedOutsideDirective } from "./clicked-outside.directive";
import { ProjectSettingsComponent } from "./project-settings/project-settings.component";
import { ReactiveFormsModule } from "@angular/forms";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { ProjectSelectComponent } from "./project-select/project-select.component";
import { DragScrollDirective } from "./drag-scroll.directive";

@NgModule({
  declarations: [
    TextAreaDirective,
    ClickedOutsideDirective,
    ProjectSettingsComponent,
    ProjectSelectComponent,
    DragScrollDirective,
  ],
  imports: [CommonModule, ReactiveFormsModule, DragDropModule, ScrollingModule],
  exports: [
    TextAreaDirective,
    ClickedOutsideDirective,
    ProjectSettingsComponent,
    ProjectSelectComponent,
    DragScrollDirective,
  ],
})
export class SharedModule {}
