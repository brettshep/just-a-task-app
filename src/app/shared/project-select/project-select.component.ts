import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Project } from "../../../../interfaces";

@Component({
  selector: "project-select",
  template: `
    <div class="card">
      <div
        class="option"
        *ngFor="let project of projects"
        [ngStyle]="{
          background: project.uid === currentProj.uid ? project.bgColor : null
        }"
        (click)="selectProject.emit(project)"
      >
        <span
          [ngStyle]="{
            background:
              project.uid === currentProj.uid
                ? project.fontColor
                : project.bgColor
          }"
        ></span>
        <h4
          [ngStyle]="{
            color:
              project.uid === currentProj.uid ? project.fontColor : 'inherit'
          }"
        >
          {{ project.name }}
        </h4>
      </div>
    </div>
  `,
  styleUrls: ["./project-select.component.sass"],
})
export class ProjectSelectComponent implements OnInit {
  @Input() projects: Project[];
  @Input() currentProj: Project;
  @Output() selectProject = new EventEmitter<Project>();

  ngOnInit(): void {}
}
