import {
  Directive,
  ElementRef,
  NgZone,
  Output,
  EventEmitter,
  Input,
} from "@angular/core";

@Directive({
  selector: "[DragScroll]",
})
export class DragScrollDirective {
  @Input() validClasses: string[] = [];
  @Output() isDragging = new EventEmitter<boolean>();

  constructor(private elemRef: ElementRef<HTMLElement>, private zone: NgZone) {}

  elem: HTMLElement;
  isDown = false;
  startX: number;
  scrollLeft: number;
  velX = 0;
  momentumID: number;

  speed: number = 1;
  friction: number = 0.95;
  // classNames = ["calendar", "row", "card-container"];

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      this.elem = this.elemRef.nativeElement;

      // mousedown
      this.elem.addEventListener("mousedown", (e) => {
        // check for middle mouse btn
        if (e.button !== 0) return;

        // only scroll if click on targets
        const target = e.target as HTMLElement;

        if (
          this.validClasses.some((className) =>
            target.classList.contains(className)
          )
        ) {
          this.isDown = true;

          this.startX = e.pageX - this.elem.offsetLeft;
          this.scrollLeft = this.elem.scrollLeft;
          this.cancelMomentumTracking();

          this.zone.run(() => {
            this.isDragging.emit(true);
          });
        }
      });

      // mouseleave
      this.elem.addEventListener("mouseleave", () => {
        this.isDown = false;

        this.zone.run(() => {
          this.isDragging.emit(false);
        });
      });

      // mouseup
      this.elem.addEventListener("mouseup", () => {
        if (!this.isDown) return;
        this.isDown = false;
        this.beginMomentumTracking();
        const sign = Math.sign(this.velX);
        this.velX = Math.min(Math.abs(this.velX), 100);
        this.velX *= sign;

        this.zone.run(() => {
          this.isDragging.emit(false);
        });
      });

      // mouse move
      this.elem.addEventListener("mousemove", (e) => {
        if (!this.isDown) return;
        e.preventDefault();
        const x = e.pageX - this.elem.offsetLeft;
        const walk = (x - this.startX) * this.speed;
        const prevScrollLeft = this.elem.scrollLeft;
        this.elem.scrollLeft = this.scrollLeft - walk;
        this.velX = this.elem.scrollLeft - prevScrollLeft;
      });

      // on scrollwheel
      this.elem.addEventListener(
        "wheel",
        (e) => {
          this.cancelMomentumTracking();
        },
        { passive: true }
      );
    });
  }

  beginMomentumTracking() {
    // start dragging momentum

    this.cancelMomentumTracking();
    this.momentumID = requestAnimationFrame(this.momentumLoop.bind(this));
  }

  cancelMomentumTracking() {
    cancelAnimationFrame(this.momentumID);
  }

  momentumLoop() {
    this.elem.scrollBy(this.velX, 0);

    this.velX *= this.friction;
    if (Math.abs(this.velX) > 1) {
      this.momentumID = requestAnimationFrame(this.momentumLoop.bind(this));
    }
  }
}
