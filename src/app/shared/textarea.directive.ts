import { Subject, Subscription } from "rxjs";
import { NgZone } from "@angular/core";
// -----------textarea.directive.ts------------

import {
  Directive,
  ElementRef,
  Input,
  Output,
  EventEmitter,
  HostListener,
} from "@angular/core";

@Directive({
  selector: "[customTextArea]",
})
export class TextAreaDirective {
  @Input() addTaskInput: boolean;
  @Input() addTaskBtn: HTMLElement;
  @Output() onSubmit = new EventEmitter<HTMLDivElement>();
  @Output() onSizeChange = new EventEmitter();

  sub: Subscription;
  currHeight: number;
  submittedText: string;
  textarea: HTMLDivElement;

  constructor(private elemRef: ElementRef, private zone: NgZone) {}

  ngAfterViewInit() {
    this.textarea = this.elemRef.nativeElement;

    // set initial text value
    this.submittedText = this.textarea.textContent;

    this.zone.runOutsideAngular(() => {
      if (this.addTaskBtn) {
        this.addTaskBtn.addEventListener("click", () => {
          if (this.textarea.textContent.trim() !== "") {
            this.zone.run(() => {
              this.onSubmit.emit(this.textarea);
            });
          }
        });
      }

      this.textarea.addEventListener("input", (e) => {
        // resize and emit if size change
        const prevHeight = this.currHeight;

        // console.log(prevHeight, this.currHeight);
        if (prevHeight !== this.currHeight) {
          this.zone.run(() => {
            this.onSizeChange.emit();
          });
        }
      });

      this.textarea.addEventListener("blur", (e) => {
        if (this.addTaskInput) {
          return;
        }

        // if empty
        if (this.textarea.textContent.trim() === "") {
          // revert state if empty and dont update
          this.textarea.textContent = this.submittedText;
        } else {
          // if not same content
          if (this.submittedText !== this.textarea.textContent) {
            this.textarea.textContent = this.textarea.textContent.trim();
            this.submittedText = this.textarea.textContent;
            this.zone.run(() => {
              this.onSubmit.emit(this.textarea);
            });
          }
        }
      });

      this.textarea.addEventListener("keydown", (e) => {
        if (e.code === "Enter" || e.code === "NumpadEnter") {
          e.preventDefault();
          e.stopPropagation();

          if (this.addTaskInput) {
            this.zone.run(() => {
              this.onSubmit.emit(this.textarea);
            });
          }
          this.textarea.blur();
        }
      });
    });
  }
}
