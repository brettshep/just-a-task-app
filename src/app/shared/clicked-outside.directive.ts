import { Directive, Output, EventEmitter, ElementRef } from "@angular/core";

@Directive({
  selector: "[ClickedOutside]",
})
export class ClickedOutsideDirective {
  @Output() exit = new EventEmitter();

  elem: HTMLElement;
  listener: Function;
  constructor(private elemRef: ElementRef<HTMLElement>) {}

  ngOnInit() {
    this.elem = this.elemRef.nativeElement;

    this.listener = (e: MouseEvent) => {
      if (!this.elem.contains(e.target as HTMLElement)) {
        // remove listener
        document.removeEventListener("click", this.listener.bind(this));
        document.removeEventListener("contextmenu", this.listener.bind(this));
        // exit
        this.exit.emit();
      }
    };

    // set timeout to prevent closing on first click
    setTimeout(() => {
      // add listener
      document.addEventListener("click", this.listener.bind(this));
      document.addEventListener("contextmenu", this.listener.bind(this));
    }, 0);
  }

  ngOnDestroy() {
    document.removeEventListener("click", this.listener.bind(this));
    document.removeEventListener("contextmenu", this.listener.bind(this));
  }
}
