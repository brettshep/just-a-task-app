import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from "@angular/core";
import { FormBuilder, Validators, FormGroup, FormArray } from "@angular/forms";
import { Project, FormProject } from "../../../../interfaces";
import { allEmojis } from "./emojis";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { ColorService } from "../../services/color.service";

@Component({
  selector: "project-settings",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <form class="card" [formGroup]="form" (submit)="onSubmit()" cdkScrollable>
      <!-- PROJECT INPUTS SECTION -->
      <section>
        <div class="title">
          <h1 class="cardName">Project Settings</h1>
          <!-- exit -->
          <span class="emoji"
            ><i class="far fa-times" (click)="exit.emit()"></i
          ></span>
        </div>
        <!-- project name -->
        <div class="inputCont">
          <label for="name">Name</label>
          <input
            id="name"
            name="name"
            formControlName="name"
            class="input grow textInput"
            placeholder="My Cool Project..."
          />
        </div>
        <!-- project Color -->
        <div class="inputCont">
          <label class="grow" for="color">Color</label>
          <div class="input colorCont">
            <input
              id="bgColor"
              name="bgColor"
              formControlName="bgColor"
              #color
              class="colorInput"
              type="color"
            />
            <div class="colorSample" [style.background]="bgColor.value"></div>
          </div>
        </div>
      </section>

      <!-- divider -->
      <div class="border"></div>

      <!-- PROJECT CATEGORIES SECTION -->
      <section formArrayName="categories">
        <div class="title">
          <h1 class="cardName">Categories</h1>
        </div>

        <!-- ng for categories -->
        <div
          class="dropList"
          cdkDropList
          [cdkDropListData]="categories.controls"
          cdkDropListLockAxis="y"
          cdkDropListOrientation="vertical"
          (cdkDropListDropped)="drop($event)"
        >
          <div
            class="category "
            *ngFor="let category of categories.controls; index as i"
            cdkDrag
          >
            <div class="inputCont" [formGroupName]="i">
              <!-- drag Handle -->
              <i class="fas fa-grip-vertical dragHandle" cdkDragHandle></i>

              <!-- text input -->
              <input
                class="input grow textInput"
                name="category-name"
                formControlName="name"
                placeholder="New Category..."
              />
              <!-- delete -->
              <i
                class="fas fa-times-circle"
                (click)="handleCategoryDelete(i)"
              ></i>
            </div>
          </div>
        </div>

        <!-- add new category -->
        <div class="category">
          <div class="inputCont">
            <!-- text input -->
            <input
              #newCatInput
              class="input grow textInput"
              placeholder="New Category..."
              (keydown)="checkNewCat($event)"
            />
            <!-- plus -->
            <i class="fas fa-plus" (click)="addNewCat(newCatInput)"></i>
          </div>
        </div>
      </section>
      <!-- divider -->
      <div class="border"></div>

      <section>
        <div class="acceptButtons">
          <button (click)="exit.emit()" type="button">Cancel</button>
          <button (click)="onSubmit()" type="button" class="accent">
            {{ project ? "Save" : "Create" }}
          </button>
        </div>
        <div class="error" *ngIf="submitted && !form.valid">
          <i class="fas fa-exclamation-triangle"></i>
          Not all information is completed!
        </div>
      </section>
    </form>

    <!----------------- CATEGORY DELETE POPUP --------------->
    <div class="categoryDeleteCont" *ngIf="categoryDeletePopup">
      <div class="card">
        <section>
          <div class="title">
            <h1 class="cardName">
              Delete
              {{ categories.at(categoryToDelete.formIndex).value.name }}
              {{ categories.at(categoryToDelete.formIndex).value.emoji }}
            </h1>
            <!-- exit -->
            <span class="emoji"
              ><i class="far fa-times" (click)="categoryDeletePopup = false"></i
            ></span>
          </div>

          <h3>What should we do with its cards?</h3>
          <div class="inputCont">
            <div
              class="check"
              [class.checked]="moveTaskOption"
              (click)="moveTaskOption = true"
            ></div>
            <label>Move cards to</label>
            <select class="grow" #catMoveSelect>
              <option value="All">Uncategorized</option>
              <ng-container *ngFor="let cat of project.categories">
                <option
                  *ngIf="cat.uid !== categoryToDelete.uid"
                  [value]="cat.uid"
                >
                  {{ cat.name }}
                </option>
              </ng-container>
            </select>
          </div>
          <div class="inputCont">
            <div
              class="check"
              [class.checked]="!moveTaskOption"
              (click)="moveTaskOption = false"
            ></div>
            <label>Delete Cards</label>
          </div>
        </section>

        <!-- divider -->
        <div class="border"></div>

        <section>
          <div class="acceptButtons">
            <button (click)="categoryDeletePopup = false" type="button">
              Cancel
            </button>
            <button
              class="red"
              (click)="onDeleteProjectCategory(catMoveSelect.value)"
              type="button"
            >
              Delete Category
            </button>
          </div>
          <div class="error" *ngIf="submitted && !form.valid">
            <i class="fas fa-exclamation-triangle"></i>
            Not all information is completed!
          </div>
        </section>
      </div>
    </div>
  `,
  styleUrls: ["./project-settings.component.sass"],
})
export class ProjectSettingsComponent implements OnInit {
  @Input() project: Project;
  @Output() exit = new EventEmitter();
  @Output() newProject = new EventEmitter<FormProject>();
  @Output() editProject = new EventEmitter<Project>();
  @Output() deleteProjectCategory = new EventEmitter<{
    deleteUID: string;
    moveToUID: string;
  }>();

  form: FormGroup;
  submitted: boolean;
  categoryDeletePopup: boolean;
  moveTaskOption: boolean = true;
  categoryToDelete: { formIndex: number; uid: string };
  emojis = allEmojis;

  constructor(private fb: FormBuilder, private colorServ: ColorService) {}

  ngOnInit(): void {
    //form
    this.form = this.fb.group({
      name: ["", [Validators.required]],
      bgColor: ["#0085FF", [Validators.required]],
      categories: this.fb.array([]),
    });

    if (this.project) {
      this.setDefaults();
    }
  }

  //--------FORM SUBMIT------
  onSubmit() {
    this.submitted = true;

    if (!this.form.valid) return;

    //editing a project
    if (this.project) {
      const data: FormProject = this.form.value;
      // name
      this.project.name = data.name;
      // bgColor
      this.project.bgColor = data.bgColor;
      // calc font color
      this.project.fontColor = this.calcFontColor(data.bgColor);
      // categories
      this.project.categories = this.categories.controls.map(
        (control) => control.value
      );

      // append new data to current project
      this.editProject.emit(this.project);
    }
    //creating a new project
    else {
      const data: FormProject = this.form.value;
      // calc font color
      data.fontColor = this.calcFontColor(data.bgColor);
      // change category order if necessary
      data.categories = this.categories.controls.map(
        (control) => control.value
      );
      this.newProject.emit(data);
    }

    // this.exit.emit();
  }

  setDefaults() {
    for (const key in this.project) {
      const formControl = this.form.get(key);
      //add arrays
      if (formControl) {
        if (key === "categories") {
          this.project.categories.forEach((val) =>
            this.categories.push(this.createCategory(val.name, val.uid))
          );
        } else {
          formControl.setValue(this.project[key]);
        }
      }
    }
  }

  // ---Category Handling----
  createCategory(name: string, uid?: string): FormGroup {
    return this.fb.group({
      name,
      uid,
    });
  }

  deleteFormCategory(i: number) {
    this.categories.removeAt(i);
  }

  onDeleteProjectCategory(catToMoveToUID: string) {
    // remove from form
    this.deleteFormCategory(this.categoryToDelete.formIndex);

    if (this.moveTaskOption) {
      // move task to category
      this.deleteProjectCategory.emit({
        deleteUID: this.categoryToDelete.uid,
        moveToUID: catToMoveToUID,
      });
    } else {
      // delete task
      this.deleteProjectCategory.emit({
        deleteUID: this.categoryToDelete.uid,
        moveToUID: "",
      });
    }

    this.exit.emit();
  }

  selectCategoryToDelete(i: number, catUID: string) {
    this.categoryToDelete = { formIndex: i, uid: catUID };
    this.categoryDeletePopup = true;
  }

  handleCategoryDelete(i: number) {
    // check if category is in current project
    const catUID: string = this.categories.at(i).value.uid;

    if (!this.project || !catUID) this.categories.removeAt(i);
    else {
      this.selectCategoryToDelete(i, catUID);
    }
  }

  checkNewCat(e: KeyboardEvent) {
    if (e.key === "Enter") {
      this.addNewCat(e.target as HTMLInputElement);
    }
  }

  addNewCat(inputElem: HTMLInputElement) {
    const name = inputElem.value;
    if (name) {
      this.categories.push(this.createCategory(name));
      inputElem.value = "";
    }
  }

  drop(event: CdkDragDrop<any[]>) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }

  calcFontColor(bgColor: string): string {
    const testColor = "#000000";
    const success = this.colorServ.calculateRatio(bgColor, testColor);
    return success ? "#000000" : "#ffffff";
  }

  //--------GETTERS------
  get name() {
    return this.form.get("name");
  }
  get bgColor() {
    return this.form.get("bgColor");
  }

  get categories(): FormArray {
    return this.form.get("categories") as FormArray;
  }
}
