import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./guards/authGuard.guard";
import { HasCurrentProjectGuard } from "./guards/has-current-project.guard";
import { InviteGuard } from "./guards/invite.guard";

const routes: Routes = [
  {
    path: "auth",
    loadChildren: () => import("./auth/auth.module").then((m) => m.AuthModule),
  },
  {
    path: "home",
    canActivate: [AuthGuard],
    loadChildren: () => import("./home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "project",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./project/project.module").then((m) => m.ProjectModule),
  },
  {
    path: "timeline",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./timeline/timeline.module").then((m) => m.TimelineModule),
  },
  {
    path: "invite/:id",
    canActivate: [InviteGuard],
    loadChildren: () =>
      import("./invite/invite.module").then((m) => m.InviteModule),
  },
  { path: "**", redirectTo: "home", pathMatch: "full" },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class AppRoutingModule {}
