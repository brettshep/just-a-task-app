import { Component, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "workspace-popup",
  template: `
    <div class="card">
      <section>
        <div class="title">
          <h1 class="cardName">Create Workspace</h1>
          <!-- exit -->
          <span class="emoji"
            ><i class="far fa-times" (click)="exit.emit()"></i
          ></span>
        </div>

        <!-- workspace name -->
        <div class="inputCont">
          <label for="name">Name</label>
          <input
            #nameInput
            class="input grow textInput"
            placeholder="Workspace Name..."
          />
        </div>
      </section>
      <!-- divider -->
      <div class="border"></div>

      <!-- accept buttons -->
      <section>
        <div class="acceptButtons">
          <button (click)="exit.emit()" type="button">Cancel</button>
          <button
            (click)="submit(nameInput.value)"
            type="button"
            class="accent"
          >
            Create
          </button>
        </div>
        <div class="error" *ngIf="submitted && !nameInput.value">
          <i class="fas fa-exclamation-triangle"></i>
          Not all information is completed!
        </div>
      </section>
    </div>
  `,
  styleUrls: ["./workspace-popup.component.sass"],
})
export class WorkspacePopupComponent {
  @Output() exit = new EventEmitter();
  @Output() newWorkspace = new EventEmitter<string>();
  submitted: boolean;

  submit(name: string) {
    this.submitted = true;
    if (name) this.newWorkspace.emit(name);
  }
}
