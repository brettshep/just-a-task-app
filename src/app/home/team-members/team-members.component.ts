import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  Output,
} from "@angular/core";
import { Member } from "../../../../interfaces";
import { EventEmitter } from "@angular/core";

@Component({
  selector: "team-members",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <!-- team members container -->
    <div class="teamCont card">
      <h2>Team Members</h2>

      <!-- members -->
      <div class="members">
        <div class="member" *ngFor="let member of members">
          <img [src]="member.profilePic" [alt]="member.name" />
          <div class="memberDetails">
            <h5>{{ member.name }}</h5>
            <h6>{{ member.email }}</h6>
          </div>
          <!-- <i class="fas fa-cog"></i> -->
        </div>
      </div>

      <!-- invite container -->
      <div class="inviteCont">
        <h4>Invite someone to the workspace.</h4>
        <form (submit)="onSendInvite($event, input)">
          <input
            #input
            type="text"
            placeholder="Email Address..."
            (input)="sendSuccess = false"
          />
          <button><i class="fas fa-paper-plane"></i></button>
        </form>
        <div class="success" *ngIf="sendSuccess">Sent Sucessfully!</div>
      </div>
    </div>
  `,
  styleUrls: ["./team-members.component.sass"],
})
export class TeamMembersComponent implements OnInit {
  @Input() set setMembers(val: { [userID: string]: Member }) {
    let members = [];
    for (const key in val) {
      if (val.hasOwnProperty(key)) {
        members.push(val[key]);
      }
    }
    this.members = members.sort((a, b) => a.name.localeCompare(b.name));
  }
  @Output() sendInvite = new EventEmitter<string>();

  members: Member[];
  sendSuccess: boolean;

  ngOnInit(): void {}

  onSendInvite(e: Event, input: HTMLInputElement) {
    e.preventDefault();
    const email = input.value.trim();
    if (email && !this.members.some((mem) => mem.email === email)) {
      if (email.match(/^\S+@\S+$/)) {
        this.sendSuccess = true;
        this.sendInvite.emit(email);
      }
    }
    input.value = "";
  }
}
