import { Component, OnInit } from "@angular/core";
import { Member, Project, FormProject, Workspace } from "../../../interfaces";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { Store } from "../store";
import { ProjectService } from "../services/project.service";
import { switchMap } from "rxjs/operators";
import { InviteService } from "../services/invite.service";
import { AuthService } from "../services/auth.service";

@Component({
  selector: "app-home",
  template: `
    <div
      class="container"
      *ngIf="currWorkspace$ | async as currWorkspace; else loading"
    >
      <!-- header to choose workspace -->
      <header>
        <div class="text">
          <span (click)="workspaceDropdown = true">
            <h1>{{ currWorkspace.name }}</h1>
            <i class="fas fa-caret-down"></i>
          </span>
          <!-- workspace dropdown -->
          <workspace-dropdown
            class="workspaceSelect"
            *ngIf="workspaceDropdown"
            [workspaces]="currWorkspaceList$ | async"
            [currWorkspace]="currWorkspace"
            (selectWorkspace)="selectWorkspace($event)"
            (newWorkspace)="newWorkspaceToggle()"
            ClickedOutside
            (exit)="workspaceDropdown = false"
          ></workspace-dropdown>
        </div>
        <div class="space"></div>
      </header>

      <!-- rest of content -->
      <div class="maxWidth">
        <!-- team members -->
        <team-members
          [setMembers]="currWorkspace.members"
          (sendInvite)="sendInvite($event, currWorkspace)"
        ></team-members>

        <!--projects -->

        <home-projects
          [projects]="projects$ | async"
          (newProject)="newProjectPopup = true"
          (goToProj)="goToProj($event)"
        ></home-projects>
      </div>

      <!-- new project popup -->
      <project-settings
        *ngIf="newProjectPopup"
        [project]="null"
        (exit)="newProjectPopup = false"
        (newProject)="createNewProject($event)"
      ></project-settings>

      <!-- workspace popup -->
      <workspace-popup
        *ngIf="workspacePopup"
        (exit)="workspacePopup = false"
        (newWorkspace)="createNewWorkspace($event)"
      ></workspace-popup>
    </div>
    <div class="spinner" *ngIf="loadingData"></div>

    <ng-template #loading>
      <div class="spinner"></div>
    </ng-template>
  `,
  styleUrls: ["./home.component.sass"],
})
export class HomeComponent implements OnInit {
  // popups
  newProjectPopup: boolean;
  workspacePopup: boolean;
  workspaceDropdown: boolean;

  // data
  currWorkspace$: Observable<Workspace>;
  currWorkspaceList$: Observable<Workspace[]>;
  projects$: Observable<Project[]>;

  // loading
  loadingData: boolean;

  constructor(
    private router: Router,
    private projectServ: ProjectService,
    private inviteServ: InviteService
  ) {}

  ngOnInit() {
    this.currWorkspace$ = this.projectServ.currWorkspace$;
    this.currWorkspaceList$ = this.projectServ.fetchWorkspaces();
    this.projects$ = this.projectServ.fetchProjects();
  }

  goToProj(e: Project) {
    this.projectServ.setCurrentProject(e);
    this.router.navigateByUrl(`project`);
  }

  async selectWorkspace(e: Workspace) {
    this.workspaceDropdown = false;
    this.loadingData = true;
    await this.projectServ.selectWorkspace(e);
    this.loadingData = false;
  }
  newWorkspaceToggle() {
    this.workspaceDropdown = false;
    // create new workspace
    this.workspacePopup = true;
  }

  async createNewWorkspace(e: string) {
    this.workspacePopup = false;
    this.loadingData = true;
    await this.projectServ.createWorkspace(e);
    this.loadingData = false;
  }

  createNewProject(e: FormProject) {
    const uid = this.projectServ.createProject(e);

    // navigate to it
    this.router.navigateByUrl(`project`);
  }

  sendInvite(e: string, workspace: Workspace) {
    this.inviteServ.invite(e, workspace);
  }
}
