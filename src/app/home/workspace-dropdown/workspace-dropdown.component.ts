import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from "@angular/core";
import { Workspace } from "../../../../interfaces";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "workspace-dropdown",
  template: `
    <div class="card">
      <!-- NgFor -->

      <div
        *ngFor="let workspace of workspaces"
        class="option"
        [class.selected]="currWorkspace === workspace"
        (click)="selectWorkspace.emit(workspace)"
      >
        <h4>
          {{ workspace.name }}
        </h4>
      </div>
      <!-- create new -->
      <div class="option" (click)="newWorkspace.emit()">
        <h4>New Workspace</h4>
        <i class="far fa-plus"></i>
      </div>
    </div>
  `,
  styleUrls: ["./workspace-dropdown.component.sass"],
})
export class WorkspaceDropdownComponent {
  @Input() workspaces: Workspace[];
  @Input() currWorkspace: Workspace;
  @Output() selectWorkspace = new EventEmitter<Workspace>();
  @Output() newWorkspace = new EventEmitter();
}
