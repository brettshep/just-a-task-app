import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from "@angular/core";
import { Project } from "../../../../interfaces";

@Component({
  selector: "home-projects",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="projects">
      <h2>Projects</h2>
      <div class="grid">
        <!-- ng for projects -->
        <div
          class="project"
          (click)="goToProj.emit(project)"
          *ngFor="let project of projects"
          [style.background]="project.bgColor"
          [style.color]="project.fontColor"
        >
          <h3>
            {{ project.name }}
            <i class="fas fa-lock private" *ngIf="project.private"></i>
          </h3>
        </div>
        <!-- add projects -->
        <button class="addProject" (click)="newProject.emit()">
          <i class="fas fa-plus"></i>
        </button>
      </div>
    </div>
  `,
  styleUrls: ["./projects.component.sass"],
})
export class ProjectsComponent implements OnInit {
  @Input() projects: Project[];

  @Output() goToProj = new EventEmitter<Project>();

  @Output() newProject = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}
}
