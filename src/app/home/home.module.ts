import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HomeComponent } from "./home.component";
import { RouterModule, Routes } from "@angular/router";
import { TeamMembersComponent } from "./team-members/team-members.component";
import { ProjectsComponent } from "./projects/projects.component";
import { SharedModule } from "../shared/shared.module";
import { WorkspaceDropdownComponent } from "./workspace-dropdown/workspace-dropdown.component";
import { WorkspacePopupComponent } from "./workspace-popup/workspace-popup.component";

export const ROUTES: Routes = [
  {
    path: "",
    component: HomeComponent,
  },
  { path: "**", redirectTo: "/home" },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), SharedModule],
  declarations: [
    HomeComponent,
    TeamMembersComponent,
    ProjectsComponent,
    WorkspaceDropdownComponent,
    WorkspacePopupComponent,
  ],
})
export class HomeModule {}
