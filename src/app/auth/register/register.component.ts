import { Component } from "@angular/core";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { FormGroup } from "@angular/forms";
import { InviteService } from "../../services/invite.service";

@Component({
  selector: "app-register",
  template: `
    <div>
      <auth-form (submitted)="RegisterUser($event)" [registerForm]="true">
        <h1>Register</h1>
        <div class="error" *ngIf="error">
          {{ error }}
        </div>
        <button class="actionBtn" type="submit">Create Account</button>
        <a routerLink="/auth/login">Have an account?</a>
      </auth-form>
    </div>
  `,
  styleUrls: ["./register.component.sass"],
})
export class RegisterComponent {
  constructor(
    private authServ: AuthService,
    private router: Router,
    private inviteServ: InviteService
  ) {}

  error: string;

  ngOnInit() {}

  async RegisterUser(event: any) {
    const { email, password, name, profilePic } = event;
    try {
      await this.authServ.createUser(email, password, name, profilePic);
      if (this.inviteServ.workspaceToAdd)
        this.router.navigate([`/invite/${this.inviteServ.workspaceToAdd}`]);
      else this.router.navigate(["/home"]);
    } catch (err) {
      this.error = err.message;
    }
  }
}
