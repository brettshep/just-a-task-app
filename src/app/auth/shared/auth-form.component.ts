import {
  Component,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Input } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "auth-form",
  styleUrls: ["auth-form.component.sass"],
  template: `
    <div class="auth-form">
      <form [formGroup]="form" (ngSubmit)="OnSubmit()">
        <ng-content select="h1"></ng-content>

        <!-- register form inputs -->
        <ng-container *ngIf="registerForm">
          <label class="marginBtm"> Profile Picture</label>
          <div class="profilePic" [class.hideBorder]="profilePicSRC">
            <input
              type="file"
              (change)="onFileChange($event)"
              formControlName="profilePic"
            />
            <img
              *ngIf="profilePicSRC; else plus"
              [src]="profilePicSRC"
              alt="profilePic"
            />
            <ng-template #plus>
              <i class="fas fa-plus"></i>
            </ng-template>
          </div>

          <label [class.isFocused]="nameFocused">
            Name
            <input
              autocomplete="name"
              (focus)="nameFocused = true"
              (blur)="nameFocused = false"
              type="text"
              formControlName="name"
            />
          </label>
        </ng-container>

        <label [class.isFocused]="emailFocused">
          Email
          <input
            autocomplete="username"
            (focus)="emailFocused = true"
            (blur)="emailFocused = false"
            type="email"
            formControlName="email"
          />
        </label>

        <label [class.isFocused]="passFocused">
          Password
          <input
            autocomplete="current-password"
            (focus)="passFocused = true"
            (blur)="passFocused = false"
            type="password"
            formControlName="password"
          />
        </label>

        <!-- login errors -->
        <div class="error" *ngIf="triedSubmit && !form.valid">
          All fields required.
        </div>
        <!-- register errors -->
        <ng-container *ngIf="registerForm">
          <div
            class="error"
            *ngIf="
              triedSubmit &&
              (!form.valid || !nameControl.value || !profilePicControl.value)
            "
          >
            All fields required.
          </div>
        </ng-container>

        <ng-content select=".error"></ng-content>

        <div class="auth-form__button">
          <ng-content select=".actionBtn"></ng-content>
        </div>

        <div class="auth-form__toggleLink">
          <ng-content select="a"></ng-content>
        </div>
      </form>

      <!-- spinner -->
      <div *ngIf="validSubmit" class="spinner"></div>
    </div>
  `,
})
export class AuthFormComponent {
  @Input() registerForm: boolean;
  @Output() submitted = new EventEmitter<FormGroup>();
  emailFocused: boolean = false;
  passFocused: boolean = false;
  nameFocused: boolean = false;
  triedSubmit: boolean;
  profilePic: File;
  profilePicSRC: string;
  validSubmit: boolean;

  form = this.fb.group({
    email: ["", Validators.email],
    password: ["", Validators.required],
    name: [""],
    profilePic: [] as File[],
  });

  constructor(private fb: FormBuilder, private cd: ChangeDetectorRef) {}

  OnSubmit() {
    this.triedSubmit = true;
    if (this.form.valid) {
      // check if registerform
      if (this.registerForm) {
        // check profilepic and name
        if (this.nameControl.value && this.profilePic) {
          let value = this.form.value;
          value.profilePic = this.profilePic;
          this.submitted.emit(value as any);
          this.validSubmit = true;
        }
      } else {
        this.validSubmit = true;
        this.submitted.emit(this.form);
      }
    }
  }

  onFileChange(e: any) {
    const files = e.target.files;
    //see if file
    if (files && files[0]) {
      const file = files[0];
      const imageType = /image.*/;
      //see if image file
      if (file.type.match(imageType)) {
        // set profile pic
        this.profilePic = file;
        // set preview
        const reader = new FileReader();
        reader.addEventListener(
          "load",
          () => {
            // convert image file to base64 string
            this.profilePicSRC = reader.result as string;
            this.cd.markForCheck();
          },
          false
        );
        reader.readAsDataURL(file);
      }
    }
  }

  get nameControl() {
    return this.form.controls["name"];
  }
  get profilePicControl() {
    return this.form.controls["profilePic"];
  }
}
