import { Component } from "@angular/core";
import { Member, Workspace, Project } from "../../interfaces";
import { ProjectService } from "./services/project.service";
import { Observable } from "rxjs";

import { AuthService } from "./services/auth.service";
import { Router, NavigationEnd } from "@angular/router";

@Component({
  selector: "app-root",
  template: `
    <div class="page">
      <ng-container *ngIf="currWorkspace$ | async as workspace">
        <ng-container>
          <dark-mode-toggle
            [style.display]="onHomePage ? 'block' : 'none'"
            [darkMode]="darkMode$ | async"
            (toggle)="toggleDarkMode($event)"
          >
          </dark-mode-toggle>
          <app-header
            *ngIf="!onInvitePage"
            [setMembers]="workspace.members"
            [currProj]="currProject$ | async"
            [currSelectedUser]="currSelectedUser$ | async"
            (selectUser)="setSelectedUser($event)"
          ></app-header>
        </ng-container>
      </ng-container>
      <router-outlet #myOutlet="outlet"></router-outlet>
    </div>
  `,
  styleUrls: ["./app.component.sass"],
  // animations: RouteAnimations
})
export class AppComponent {
  constructor(
    private projectServ: ProjectService,
    private authServ: AuthService,
    private router: Router
  ) {}

  currWorkspace$: Observable<Workspace>;
  currProject$: Observable<Project>;
  currSelectedUser$: Observable<string>;
  onInvitePage: boolean;
  onHomePage: boolean;
  darkMode$: Observable<boolean>;

  toggleDarkMode(e) {
    this.authServ.toggleDarkMode(e);
  }

  ngOnInit() {
    // fetch darkmode
    this.darkMode$ = this.authServ.fetchDarkMode();

    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.onInvitePage = e.url.includes("invite");
        this.onHomePage = e.url.includes("home");
      }
    });

    // subscribe to user
    this.authServ.user$.subscribe();

    // subscribe to all user workspaces
    this.projectServ.appFetchWorkspaces().subscribe();

    // fetch current workspace
    this.currWorkspace$ = this.projectServ.currWorkspace$;

    // initiate currworkspace varialbe
    this.projectServ.appFetchProjects().subscribe();

    // set current project
    this.currProject$ = this.projectServ.currProject$;

    // set curr selected user
    this.currSelectedUser$ = this.projectServ.currSelectedUser$;
  }

  setSelectedUser(uid: string) {
    this.projectServ.currSelectedUser$.next(uid);
  }
}
