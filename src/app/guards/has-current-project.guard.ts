import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { ProjectService } from "../services/project.service";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class HasCurrentProjectGuard  {
  constructor(private projectServ: ProjectService, private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const valid = !!this.projectServ.currProject$.value;
    if (!valid) {
      this.router.navigate([`home`]);
    }
    return valid;
  }
}
