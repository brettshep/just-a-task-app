import { Injectable, inject } from "@angular/core";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { AuthService } from "../services/auth.service";

@Injectable()
export class AuthGuard {
  constructor(
    private authService: AuthService = inject(AuthService),
    private router: Router = inject(Router)
  ) {}

  canActivate() {
    return this.authService.authState$.pipe(
      map((user) => {
        if (!user) {
          this.router.navigate([`auth/login`]);
        }
        return !!user;
      })
    );
  }
}
