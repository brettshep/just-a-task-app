import { Injectable, inject } from "@angular/core";
import {
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot,
} from "@angular/router";
import { map } from "rxjs/operators";
import { AuthService } from "../services/auth.service";
import { InviteService } from "../services/invite.service";

@Injectable({
  providedIn: "root",
})
export class InviteGuard {
  constructor(
    private authService: AuthService = inject(AuthService),
    private router: Router = inject(Router),
    private inviteServ: InviteService = inject(InviteService)
  ) {}

  canActivate(route: ActivatedRouteSnapshot) {
    return this.authService.authState$.pipe(
      map((user) => {
        const inviteUID = route.paramMap.get("id");
        if (!user) {
          this.inviteServ.workspaceToAdd = inviteUID;
          this.router.navigate([`auth/register`]);
        }
        return !!user;
      })
    );
  }
}
