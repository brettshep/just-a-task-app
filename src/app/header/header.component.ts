import {
  Component,
  Input,
  ChangeDetectionStrategy,
  Output,
} from "@angular/core";
import { Member, Project } from "../../../interfaces";
import { Router } from "@angular/router";
import { EventEmitter } from "@angular/core";

@Component({
  selector: "app-header",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="container">
      <div class="card">
        <!-- nav bar -->
        <nav>
          <ul>
            <li>
              <a routerLink="/home" routerLinkActive="active"
                ><i class="fas fa-home-lg-alt"></i>
              </a>
            </li>
            <li *ngIf="currProj">
              <a
                routerLink="/project"
                routerLinkActive="active"
                (click)="goCurrProj()"
                ><i class="fas fa-check-circle accentCol"></i>
              </a>
            </li>
            <li *ngIf="currProj">
              <a routerLink="/timeline" routerLinkActive="active"
                ><i class="fas fa-alarm-clock redCol"></i>
              </a>
            </li>
          </ul>
        </nav>

        <!-- memebers list -->
        <img
          *ngFor="let member of members"
          [class.selected]="currSelectedUser === member.email"
          (click)="setUser(member.email)"
          [src]="member.profilePic"
          [alt]="member.name"
        />
      </div>
    </div>
  `,
  styleUrls: ["./header.component.sass"],
})
export class HeaderComponent {
  @Input() set setMembers(val: { [userID: string]: Member }) {
    let members = [];
    for (const key in val) {
      if (val.hasOwnProperty(key)) {
        members.push(val[key]);
      }
    }
    this.members = members.sort((a, b) => a.name.localeCompare(b.name));
  }

  @Input() currSelectedUser: string;
  @Input() currProj: Project;

  @Output() selectUser = new EventEmitter<string>();

  members: Member[];
  constructor(private router: Router) {}

  // only availbel if currProj is available
  goCurrProj() {
    this.router.navigateByUrl(`project`);
  }

  setUser(uid: string) {
    if (this.currSelectedUser === uid) {
      this.selectUser.emit("");
    } else {
      this.selectUser.emit(uid);
    }
  }
}
