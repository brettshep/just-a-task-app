import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
// import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";
import { AppRoutingModule } from "./routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Store } from "./store";

// Firebase
import { HeaderComponent } from "./header/header.component";
import { DarkModeToggleComponent } from "./dark-mode-toggle/dark-mode-toggle.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { initializeApp, provideFirebaseApp } from "@angular/fire/app";
import { provideAuth, getAuth } from "@angular/fire/auth";
import { provideFirestore, getFirestore } from "@angular/fire/firestore";
import { provideFunctions, getFunctions } from "@angular/fire/functions";
import { provideStorage, getStorage } from "@angular/fire/storage";

const config = {
  apiKey: "AIzaSyBi8l1yqp67ZTa_eICpEw2lymvo88Ir4_0",
  authDomain: "just-a-task-app.firebaseapp.com",
  projectId: "just-a-task-app",
  storageBucket: "just-a-task-app.appspot.com",
  messagingSenderId: "183499448188",
  appId: "1:183499448188:web:64ea2a4575587eeed04ef9",
  measurementId: "G-Z9NGR9RKWJ",
};

@NgModule({
  declarations: [AppComponent, HeaderComponent, DarkModeToggleComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production,
    }),
    provideFirebaseApp(() => initializeApp(config)),
    provideAuth(() => getAuth()),
    provideFirestore(() => getFirestore()),
    provideFunctions(() => getFunctions()),
    provideStorage(() => getStorage()),
  ],
  providers: [
    // { provide: ORIGIN, useValue: "http://localhost:5001" }, //functions emulator
    // {
    //   provide: SETTINGS,
    //   useValue: {
    //     host: "localhost:8080",
    //     ssl: false,
    //   },
    // }, //firestore emulator
    Store,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
