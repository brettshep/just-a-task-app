import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "dark-mode-toggle",
  template: `
    <button class="container" [class.active]="_darkMode" (click)="toggleMode()">
      <span></span>
      <i class="fas fa-sun"></i>
      <i class="fas fa-moon"></i>
    </button>
  `,
  styleUrls: ["./dark-mode-toggle.component.sass"],
})
export class DarkModeToggleComponent {
  @Input() set darkMode(val: boolean) {
    this._darkMode = val;
    // change css
    if (val) {
      document.documentElement.setAttribute("data-theme", "dark");
    } else {
      document.documentElement.removeAttribute("data-theme");
    }
  }

  @Output() toggle = new EventEmitter<boolean>();
  _darkMode: boolean;

  toggleMode() {
    this.darkMode = !this._darkMode;
    // emit
    this.toggle.emit(this._darkMode);
  }
}
