import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from "@angular/core";
import { Category } from "../../../../interfaces";

@Component({
  selector: "category-dropdown",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card">
      <!-- NgFor -->

      <div
        *ngFor="let category of options"
        class="option"
        [class.selected]="currOption === category.uid"
        (click)="newOption.emit(category.uid)"
      >
        <h4>
          {{ category.name }}
        </h4>
      </div>
    </div>
  `,
  styleUrls: ["./dropdown.component.sass"],
})
export class DropdownComponent {
  @Input() options: Category[];
  @Input() currOption: string;
  @Output() newOption = new EventEmitter<string>();
}
