import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  HostListener,
} from "@angular/core";
import {
  CalendarDay,
  Project,
  ProjectList,
  CalendarCard,
  Workspace,
  Member,
} from "../../../interfaces";
import { ProjectService } from "../services/project.service";
import { Observable, Subject, BehaviorSubject, combineLatest } from "rxjs";
import { map, take, takeWhile, tap } from "rxjs/operators";

import { Card, CategoryList, Task } from "../../../interfaces";

@Component({
  selector: "app-timeline",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div
      class="container"
      *ngIf="currProject$ | async as currentProj; else loading"
    >
      <!-- Project Title Header -->
      <header>
        <div class="space calendar" [class.open]="taskBarOpen">
          <i
            class="fas fa-arrow-to-right"
            (click)="taskBarOpen = true"
            *ngIf="!taskBarOpen"
          ></i>
        </div>
        <div
          class="text"
          *ngIf="currDateRangeName$ | async as currDateRangeName"
        >
          <div class="text-top">
            <i class="far fa-chevron-left" (click)="moveDate(-1)"></i>
            <h1>{{ currDateRangeName }}</h1>
            <i class="far fa-chevron-right" (click)="moveDate(1)"></i>
          </div>
          <!-- *ngIf="this.selectedDayUID !== todayUID" -->
          <button (click)="goToToday()">
            Today <i class="fas fa-chevron-right"></i>
          </button>
        </div>
        <div class="space"></div>
      </header>

      <div
        class="group"
        [class.open]="taskBarOpen"
        *ngIf="cards$ | async as cards; else loading"
      >
        <!-- TaskBar -->
        <project-taskbar
          [style.display]="taskBarOpen ? 'block' : 'none'"
          [projects]="projects$ | async"
          [members]="members$ | async"
          [currentProj]="currentProj"
          (selectProject)="setCurrentProject($event)"
          [categoryList]="categoryList$ | async"
          [currCatUID]="currCatUID$ | async"
          [todayUID]="todayUID"
          [cards]="cards"
          [selectedDayUID]="selectedDayUID"
          (selectCategory)="setNextCategory($event)"
          (toggleScheduleTask)="toggleScheduleTask($event, currentProj)"
          (exit)="taskBarOpen = false"
        ></project-taskbar>

        <!-- Calendar -->
        <div
          class="calendarCont"
          [ngClass]="{
            dragging: dragState
          }"
        >
          <!-- taskbar shadow -->
          <div class="taskBarShadow" *ngIf="taskBarOpen"></div>
          <ng-container
            *ngIf="calendarDays$ | async as calendarDays; else loading"
          >
            <calendar
              class="card-container"
              [todayUID]="todayUID"
              [workspace]="currWorkspace$ | async"
              [currSelectedUser]="currSelectedUser$ | async"
              [calendarDays]="calendarDays"
              [projectList]="projectList$ | async"
              [categoryList]="categoryList$ | async"
              [selectedDayUID]="selectedDayUID"
              [setscrollToPosition]="scrollToPosition$ | async"
              (dragState)="dragState = $event"
              (selectNewCalDay)="selectedDayUID = $event"
              (checkTask)="checkTask($event)"
              (updateCalCardOrder)="updateCalCardOrder($event)"
              (updateCalCardNewDay)="updateCalCardNewDay($event)"
            ></calendar>
          </ng-container>
        </div>
      </div>
    </div>
    <ng-template #loading>
      <div class="spinner"></div>
    </ng-template>
  `,
  styleUrls: ["./timeline.component.sass"],
})
export class TimelineComponent implements OnInit {
  constructor(private projectServ: ProjectService) {}

  //---- const vars
  calDayRange: number = 10;

  // ----local state
  selectedDayUID: number;
  dragState: boolean;
  todayUID: number;
  originalDate: Date;

  // ----popups
  taskBarOpen: boolean = true;

  // ----local observables
  currDate$: BehaviorSubject<Date> = new BehaviorSubject(null);
  currCatUID$: BehaviorSubject<string> = new BehaviorSubject("");
  scrollToPosition$ = new Subject<any>();
  currDateRangeName$: Observable<string>;

  // ---data observables
  currWorkspace$: Observable<Workspace>;
  projectList$: Observable<ProjectList>;
  projects$: Observable<Project[]>;
  currProject$: BehaviorSubject<Project>;
  categoryList$: Observable<CategoryList>;
  allCards$: Observable<Card[]>;
  cards$: Observable<Card[]>;
  calendarDays$: Observable<CalendarDay[]>;
  currSelectedUser$: Observable<string>;
  members$: Observable<Member[]>;

  ngOnInit(): void {
    this.currWorkspace$ = this.projectServ.currWorkspace$;
    this.currSelectedUser$ = this.projectServ.currSelectedUser$;

    // fetch projects
    this.projects$ = this.projectServ.fetchProjects();

    // get memebers form current workspace
    this.members$ = this.projectServ.currWorkspace$.pipe(
      map((workspace) => {
        if (workspace) {
          let members = [];
          for (const key in workspace.members) {
            if (workspace.members.hasOwnProperty(key)) {
              members.push(workspace.members[key]);
            }
          }
          return members;
        } else {
          return [];
        }
      })
    );

    // set list of projects
    this.projectList$ = this.projects$.pipe(
      map((project) => {
        let list: ProjectList = {};
        project.forEach((project) => {
          list[project.uid] = project;
        });
        return list;
      })
    );

    // set current project
    this.currProject$ = this.projectServ.currProject$;

    this.currCatUID$.next(this.currProject$.value?.categories[0]?.uid);

    //subscribe to the current project until it is initally set and not null
    this.currProject$
      .pipe(
        tap((project) => {
          if (project && project.categories.length && !this.currCatUID$.value) {
            this.currCatUID$.next(project.categories[0]?.uid);
          }
        }),
        takeWhile((project) => !project, true)
      )
      .subscribe();

    // create category list
    this.categoryList$ = this.projects$.pipe(
      map((projects) => {
        let list: CategoryList = {};
        if (projects?.length) {
          for (const project of projects) {
            for (const cat of project.categories) {
              list[cat.uid] = cat;
            }
          }
        }
        return list;
      })
    );

    this.originalDate = new Date();

    // set current date
    this.goToToday();

    // fetch all cards
    this.allCards$ = this.projectServ.fetchCards();

    // fetch cards and filter based on category
    this.cards$ = combineLatest([
      this.projectServ.fetchCards(),
      this.currCatUID$,
      this.projectServ.currSelectedUser$,
    ]).pipe(
      map((data) => {
        if (!data[0]) return [];
        let cards = [...data[0]];
        const catUID = data[1];
        const selectedUser = data[2];

        //lots of filtering here

        //first filter all cards that are not in the current category
        cards = cards.filter((card) => card.categoryID === catUID);

        //now remove all tasks in cards that are marked as complete
        cards = cards.map((_card) => {
          const card = { ..._card };
          card.tasks = card.tasks.filter((task) => !task.completed);
          return card;
        });

        //if a selected user is set, filter all the tasks in cards that don't have that user assigned
        if (selectedUser) {
          cards = cards.map((_card) => {
            const card = { ..._card };
            card.tasks = card.tasks.filter(
              (task) => task.assignedUserUID === selectedUser
            );
            return card;
          });
        }

        //first remove all tasks in cards that don't have any tasks
        cards = cards.filter((card) => card.tasks.length);

        return cards.sort((a, b) => a.order - b.order);
      })
    );

    // fetch calendar days
    // this.calendarDays$ = this.projectServ.fetchCalendarDays(
    //   this.currDate$,
    //   this.calDayRange
    // )

    this.calendarDays$ = this.projectServ.fetchCalendarDays(
      this.currDate$,
      this.calDayRange
    );

    // set date range string
    this.currDateRangeName$ = this.currDate$.pipe(
      map((date) => {
        if (date) {
          // start date
          const startDate = new Date(date.getTime());
          startDate.setDate(startDate.getDate() - this.calDayRange);
          // end date
          const endDate = new Date(date.getTime());
          endDate.setDate(endDate.getDate() + this.calDayRange);

          const startDateMonth = startDate.getMonth();
          const startDateDate = startDate.getDate();

          const endDateMonth = endDate.getMonth();
          const endDateDate = endDate.getDate();

          return `${this.projectServ.months[startDateMonth]} ${startDateDate} - ${this.projectServ.months[endDateMonth]} ${endDateDate}`;
        } else {
          return "";
        }
      })
    );
  }

  // -----Day Handling----

  moveDate(dir: number) {
    this.selectedDayUID = null;
    const newDate = new Date(this.currDate$.value.getTime());
    newDate.setDate(newDate.getDate() + dir * this.calDayRange * 2);
    this.currDate$.next(newDate);

    // // scroll to center
    // this.scrollToPosition$.next(0);
  }

  goToToday() {
    // set current date
    const currentDate = new Date(this.originalDate.getTime());
    currentDate.setHours(0, 0, 0, 0);
    this.currDate$.next(currentDate);
    this.todayUID = currentDate.getTime();

    // scroll to center
    this.scrollToPosition$.next({});

    // set current selected date
    this.selectedDayUID = currentDate.getTime();
  }

  //-----Project and Category Selection-----

  setCurrentProject(e: Project) {
    // set current caetegory to first category
    this.currCatUID$.next(e.categories[0]?.uid);

    // set next project
    this.projectServ.setCurrentProject(e);
  }

  setNextCategory(uid: string) {
    this.currCatUID$.next(uid);
  }

  //------ Task Scheduling--------

  toggleScheduleTask(e: { task: Task; card: Card }, project: Project) {
    if (!this.selectedDayUID) return;

    let setToDateUID: number;
    let rescheduling: boolean;
    //already scheduled
    if (e.task.scheduledDayUID) {
      //reschedule to selected day
      //delete previous card and update to new day
      if (e.task.scheduledDayUID !== this.selectedDayUID) {
        setToDateUID = this.selectedDayUID;
        rescheduling = true;
      }
      //remove from selected day
      else {
        setToDateUID = null;
      }
      //set to selected day
    } else {
      setToDateUID = this.selectedDayUID;
    }

    let dayName = "";
    if (setToDateUID) {
      const setDate = new Date(setToDateUID);
      const monthName = this.projectServ.months[setDate.getMonth()];
      const date = setDate.getDate();
      dayName = `${monthName} ${date}`;
    }

    this.projectServ.toggleScheduleTask(
      e.task,
      e.card,
      project,
      setToDateUID,
      dayName,
      this.currDate$.value.getTime(),
      rescheduling
    );
  }

  updateCalCardOrder(e: { cards: CalendarCard[]; dayIndex: number }) {
    this.projectServ.updateCalendCardOrder(
      this.currDate$.value.getTime(),
      e.dayIndex,
      e.cards
    );
  }

  updateCalCardNewDay(e: {
    cardsPrev: CalendarCard[];
    cards: CalendarCard[];
    dayIndexPrev: number;
    dayIndex: number;
    calCardIndex: number;
  }) {
    this.projectServ.updateCalCardNewDayDrag(
      this.currDate$.value.getTime(),
      e.cardsPrev,
      e.cards,
      e.calCardIndex,
      e.dayIndexPrev,
      e.dayIndex
    );
  }

  checkTask(e: { card: CalendarCard; cardIndex: number; dayIndex: number }) {
    this.projectServ.checkCalendarCard(
      this.currDate$.value.getTime(),
      e.card,
      e.cardIndex,
      e.dayIndex
    );
  }

  changeCategoryIndex(dir: number) {
    const currCatUID = this.currCatUID$.value;
    this.currProject$
      .pipe(
        take(1),
        tap((project) => {
          const currCatIndex = project.categories.findIndex(
            (cat) => cat.uid === currCatUID
          );

          let nextCatIndex = currCatIndex + dir;

          // if next index is out of bounds, loop back to beginning
          if (nextCatIndex < 0) {
            nextCatIndex = project.categories.length - 1;
          } else if (nextCatIndex >= project.categories.length) {
            nextCatIndex = 0;
          }

          const nextCatUID = project.categories[nextCatIndex].uid;
          this.currCatUID$.next(nextCatUID);
        })
      )
      .subscribe();
  }

  //setup a hostlistener on the window object to listen for arrow keys
  //when arrow keys are pressed, move the category selection

  @HostListener("window:keydown", ["$event"])
  keyEvent(event: KeyboardEvent) {
    //ensure user is not focused on an input
    const activeElement = document.activeElement;
    if (activeElement.tagName === "INPUT") return;

    if (event.key === "ArrowLeft") {
      event.preventDefault();
      this.changeCategoryIndex(-1);
    }

    if (event.key === "ArrowRight") {
      event.preventDefault();
      this.changeCategoryIndex(1);
    }
  }
}
