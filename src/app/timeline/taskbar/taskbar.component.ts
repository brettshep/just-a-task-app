import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from "@angular/core";
import {
  Project,
  Card,
  Category,
  CategoryList,
  Task,
  Member,
} from "../../../../interfaces";

@Component({
  selector: "project-taskbar",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <!-- task bar -->
    <div class="taskBarCont">
      <!-- top controls -->
      <div class="topControls">
        <!-- close -->
        <i class="fas fa-arrow-to-left close" (click)="exit.emit()"></i>

        <!-- project name -->
        <div class="projectName">
          <span (click)="projectSelectPopup = true">
            <h1>{{ currentProj.name }}</h1>
            <i class="fas fa-caret-down"></i>
          </span>
          <project-select
            class="projectSelect"
            *ngIf="projectSelectPopup"
            [projects]="projects"
            [currentProj]="currentProj"
            (selectProject)="onSelectProject($event)"
            ClickedOutside
            (exit)="projectSelectPopup = false"
          ></project-select>
        </div>
        <!-- project category -->
        <div class="projectCategory">
          <div class="projectCategoryText" (click)="openCatPopup()">
            <span>
              {{ categoryList[currCatUID]?.name || "No Categories" }}
            </span>
            <i
              class="fas fa-caret-down"
              *ngIf="currentProj.categories.length"
            ></i>
          </div>
          <category-dropdown
            class="categorySelect"
            *ngIf="categorySelectPopup"
            [options]="currentProj.categories"
            [currOption]="currCatUID"
            (newOption)="onSelectCategory($event)"
            ClickedOutside
            (exit)="categorySelectPopup = false"
          ></category-dropdown>
        </div>
      </div>
      <!-- taskbar gradient -->
      <div class="gradientOverlay"></div>
      <!-- taskbar cards -->
      <div class="taskBar">
        <div class="card cardPadding" *ngFor="let card of cards">
          <!-- title -->
          <div class="title">
            <h1 class="cardName">{{ card.title }}</h1>
          </div>
          <!-- tasks -->
          <div class="taskList">
            <!-- ngfor tasks -->
            <div
              *ngFor="let task of card.tasks"
              class="task hover"
              (click)="
                toggleScheduleTask.emit({
                  task: task,
                  card: card,
                })
              "
            >
              <div class="textarea">
                {{ task.name }}
              </div>

              <div class="empty"></div>
              <div class="task-rightside">
                <span
                  class="scheduled"
                  *ngIf="task.scheduledDayName; else clock"
                  [ngStyle]="{
                    color:
                      task.scheduledDayUID === selectedDayUID
                        ? currentProj.bgColor
                        : 'inherit'
                  }"
                  >{{
                    task.scheduledDayUID === todayUID
                      ? "Today"
                      : task.scheduledDayName
                  }}</span
                >
                <img
                  class="assigned"
                  *ngIf="task.assignedUserUID"
                  [src]="findProfilePic(task.assignedUserUID)"
                />
              </div>
              <ng-template #clock>
                <i class="fas fa-alarm-clock clock"></i>
              </ng-template>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./taskbar.component.sass"],
})
export class TaskbarComponent implements OnInit {
  @Input() selectedDayUID: number;
  @Input() todayUID: number;
  @Input() projects: Project[];
  @Input() members: Member[];
  @Input() currentProj: Project;
  @Input() categoryList: CategoryList;
  @Input() currCatUID: string;
  @Input() cards: Card[];
  @Output() selectProject = new EventEmitter<Project>();
  @Output() selectCategory = new EventEmitter<string>();
  @Output() toggleScheduleTask = new EventEmitter<{
    task: Task;
    card: Card;
  }>();
  @Output() exit = new EventEmitter();

  projectSelectPopup: boolean;
  categorySelectPopup: boolean;
  today: number;

  onSelectProject(proj: Project) {
    this.selectProject.emit(proj);
    this.projectSelectPopup = false;
  }
  onSelectCategory(cat: string) {
    this.selectCategory.emit(cat);
    this.categorySelectPopup = false;
  }

  openCatPopup() {
    if (this.currentProj.categories.length) {
      this.categorySelectPopup = true;
    }
  }

  findProfilePic(email: string) {
    return this.members.find((member) => member.email === email).profilePic;
  }

  constructor() {}

  ngOnInit(): void {}
}
