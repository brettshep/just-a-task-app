import { CategoryList, Member, Workspace } from "../../../../../interfaces";
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from "@angular/core";
import { ProjectList, CalendarCard } from "../../../../../interfaces";

@Component({
  selector: "calendar-card",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div
      class="card"
      cdkDrag
      (cdkDragStarted)="startedDayDrag()"
      (cdkDragEnded)="dragState.emit(false)"
    >
      <img
        class="member-icon"
        *ngIf="card.task.assignedUserUID"
        [src]="assignedMember?.profilePic"
      />
      <!-- title bar -->
      <div class="title">
        <div class="category">
          {{ card.cardName }}
        </div>

        <div
          class="project"
          [style.color]="projectList[card.projectUID]?.bgColor"
        >
          {{ categoryList[card.categoryID]?.name || "Unknown" }}
        </div>
      </div>

      <!-- task -->
      <div class="task">
        <i
          class="far fa-circle checkbox"
          (click)="checkTask.emit()"
          [ngStyle]="{
            color: card.task.completed
              ? projectList[card.projectUID].bgColor
              : '',
            backgroundColor: card.task.completed
              ? projectList[card.projectUID].bgColor
              : ''
          }"
        >
          <!-- checkmark -->
          <i
            class="fas fa-check checkmark"
            *ngIf="card.task.completed"
            [ngStyle]="{
              color: projectList[card.projectUID].fontColor,
            }"
          ></i>
        </i>

        <div class="textarea">
          {{ card.task.name }}
        </div>
        <!-- <div class="empty"></div> -->
        <!-- <i class="fas fa-grip-vertical dragHandle" cdkDragHandle></i> -->
      </div>
    </div>
  `,
  styleUrls: ["./card.component.sass"],
})
export class CardComponent {
  @Input() workspace: Workspace;
  @Input() categoryList: CategoryList;
  @Input() projectList: ProjectList;
  @Input() card: CalendarCard;
  @Output() dragState = new EventEmitter<boolean>();
  @Output() checkTask = new EventEmitter();

  startedDayDrag() {
    const target = document.querySelector(".cdk-drag-placeholder");
    target.classList.add("dayCal");
    this.dragState.emit(true);
  }

  get assignedMember(): Member {
    if (!this.card.task.assignedUserUID) return null;

    //search throguh objects and find member via email
    const memberArr = Object.values(this.workspace.members);
    return memberArr.find(
      (member) => member.email === this.card.task.assignedUserUID
    );
  }
}
