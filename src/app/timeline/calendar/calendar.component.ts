import {
  Component,
  ChangeDetectionStrategy,
  Input,
  ViewChild,
} from "@angular/core";
import {
  CalendarDay,
  ProjectList,
  CalendarCard,
  Workspace,
} from "../../../../interfaces";
import { Output, EventEmitter, ElementRef } from "@angular/core";
import { CategoryList } from "../../../../interfaces";

import {
  moveItemInArray,
  CdkDragDrop,
  transferArrayItem,
} from "@angular/cdk/drag-drop";

@Component({
  selector: "calendar",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <!-- CalendarDrag -->
    <div
      class="calendar"
      cdkScrollable
      #calendar
      DragScroll
      [validClasses]="draggableClasses"
    >
      <div cdkDropListGroup class="listGroup">
        <div
          *ngFor="
            let calDay of calendarDays;
            index as dayIndex;
            trackBy: myTrackByFunction
          "
          class="row"
        >
          <!-- data -->
          <div class="date-container">
            <div
              class="date"
              [ngClass]="{
                selected: selectedDayUID === calDay.uid,
                today: todayUID === calDay.uid
              }"
              (click)="selectNewCalDay.emit(calDay.uid)"
            >
              <h3>{{ dayNames[calDay.dayNumber] }}</h3>
              <h2>{{ calDay.dateNumber }}</h2>
            </div>
          </div>

          <!-- border line -->
          <div class="border"></div>

          <!-- cards -->
          <!-- droplist container for calendarCards -->
          <div
            class="card-container"
            cdkDropList
            [cdkDropListData]="{
              cards: calDay.calendarCards,
              dayIndex: dayIndex
            }"
            (cdkDropListDropped)="drop($event)"
          >
            <calendar-card
              *ngFor="let calCard of calDay.calendarCards; index as i"
              [workspace]="workspace"
              [projectList]="projectList"
              [card]="calCard"
              [categoryList]="categoryList"
              (dragState)="dragState.emit($event)"
              (checkTask)="
                checkTask.emit({
                  card: calCard,
                  cardIndex: i,
                  dayIndex: dayIndex
                })
              "
              [class.transparent]="
                calCard.task.completed ||
                (currSelectedUser &&
                  calCard.task.assignedUserUID !== currSelectedUser)
              "
            >
            </calendar-card>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./calendar.component.sass"],
})
export class CalendarComponent {
  @Input() workspace: Workspace;
  @Input() todayUID: number;
  @Input() currSelectedUser: string;
  @Input() calendarDays: CalendarDay[];
  @Input() projectList: ProjectList;
  @Input() selectedDayUID: number;
  @Input() categoryList: CategoryList;
  @Input() set setscrollToPosition(val: any) {
    this.scrollToPosition();
  }
  @Output() selectNewCalDay = new EventEmitter<number>();
  @Output() dragState = new EventEmitter<boolean>();
  @Output() checkTask = new EventEmitter<{
    card: CalendarCard;
    cardIndex: number;
    dayIndex: number;
  }>();
  @Output() updateCalCardOrder = new EventEmitter<{
    cards: CalendarCard[];
    dayIndex: number;
  }>();
  @Output() updateCalCardNewDay = new EventEmitter<{
    cardsPrev: CalendarCard[];
    cards: CalendarCard[];
    calCardIndex: number;
    dayIndexPrev: number;
    dayIndex: number;
  }>();

  @ViewChild("calendar", { static: true })
  calendarRef: ElementRef<HTMLDivElement>;

  dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  rowWidth = 270;
  draggableClasses = ["calendar", "row", "card-container"];

  ngAfterViewInit() {
    this.scrollToPosition();
  }

  scrollToPosition() {
    const calScrollWidth = this.calendarRef.nativeElement.scrollWidth;
    const calWidth = this.calendarRef.nativeElement.offsetWidth;

    // if (val === 0) {

    this.calendarRef.nativeElement.scrollLeft =
      calScrollWidth / 2 - calWidth / 2;
    // } else {
    //   this.calendarRef.nativeElement.scrollLeft = 0;
    // }
  }

  drop(
    event: CdkDragDrop<{
      cards: CalendarCard[];
      dayIndex: number;
    }>
  ) {
    // moveItemInArray(list, event.previousIndex, event.currentIndex);
    const { cards, dayIndex } = event.container.data;

    if (event.previousContainer === event.container) {
      // moving in same day
      moveItemInArray(cards, event.previousIndex, event.currentIndex);
      this.updateCalCardOrder.emit({
        cards,
        dayIndex,
      });
    }
    //move to differerent day
    else {
      const cardsPrev = (event.previousContainer.data as any).cards;
      const dayIndexPrev = (event.previousContainer.data as any).dayIndex;
      transferArrayItem(
        cardsPrev,
        cards,
        event.previousIndex,
        event.currentIndex
      );
      this.updateCalCardNewDay.emit({
        cardsPrev,
        cards,
        dayIndexPrev,
        dayIndex,
        calCardIndex: event.currentIndex,
      });
    }
  }

  myTrackByFunction(index: number, calDay: CalendarDay): number {
    return calDay.uid;
  }

  // ngDoCheck() {
  //   console.log("check");
  // }
}
