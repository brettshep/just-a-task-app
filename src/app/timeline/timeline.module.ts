import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TimelineComponent } from "./timeline.component";

import { RouterModule, Routes } from "@angular/router";
import { CalendarComponent } from "./calendar/calendar.component";
import { SharedModule } from "../shared/shared.module";
import { CardComponent } from "./calendar/card/card.component";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { TaskbarComponent } from "./taskbar/taskbar.component";
import { DropdownComponent } from "./dropdown/dropdown.component";
export const ROUTES: Routes = [
  {
    path: "",
    component: TimelineComponent,
  },
  { path: "**", redirectTo: "/home" },
];

@NgModule({
  declarations: [
    TimelineComponent,
    CalendarComponent,
    CardComponent,
    TaskbarComponent,
    DropdownComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    SharedModule,
    DragDropModule,
    ScrollingModule,
  ],
})
export class TimelineModule {}
