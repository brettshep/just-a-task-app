import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
} from "@angular/core";

@Component({
  selector: "project-settings-popup",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: ` <div
    class="settingPopup card"
    ClickedOutside
    (exit)="exit.emit()"
  >
    <div class="option" (click)="onOpenSettings()">
      Project Settings
      <i class="fas fa-cog optionIcon"></i>
    </div>
    <button class="option archive" (click)="onDeleteProject()">
      Delete Project
      <i class="fas fa-archive"></i>
    </button>
  </div>`,
  styleUrls: ["./project-settings-popup.component.sass"],
})
export class ProjectSettingsPopupComponent implements OnInit {
  @Output() exit = new EventEmitter();
  @Output() openSettings = new EventEmitter();
  @Output() deleteProject = new EventEmitter();

  onOpenSettings() {
    this.openSettings.emit();
    this.exit.emit();
  }

  constructor() {}

  ngOnInit(): void {}

  onDeleteProject() {
    const confirmDelete = confirm(
      "Are you sure you want to delete this project?"
    );

    if (!confirmDelete) {
      return;
    }

    this.deleteProject.emit();
    this.exit.emit();
  }
}
