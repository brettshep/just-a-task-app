import { Directive, ElementRef, Input } from "@angular/core";
import { Subscription, Subject } from "rxjs";

@Directive({
  selector: "[appLayout]",
})
export class LayoutDirective {
  @Input() gap: number;
  @Input() repack: Subject<any>;
  @Input() measureElem: HTMLElement;

  // observer : MutationObserver;

  columnHeights = [];
  maxColumns = 1;
  elemColumns: HTMLElement[][];
  colWidth: number;
  repackSub: Subscription;
  observer = new MutationObserver(this.pack.bind(this));
  observerOptions: MutationObserverInit = {
    childList: true,
    attributes: false,
    subtree: false,
  };

  constructor(private contElem: ElementRef<HTMLElement>) {}

  ngOnInit() {
    this.repackSub = this.repack.subscribe(() => {
      this.pack();
    });
  }

  ngAfterViewInit() {
    this.pack();

    // add mutation observer for change in cards
    this.observer.observe(this.contElem.nativeElement, this.observerOptions);
  }

  ngOnDestroy() {
    this.repackSub.unsubscribe();
    this.observer.disconnect();
  }

  pack(e?: MutationCallback) {
    // console.log(e);
    const maxWidth = this.measureElem.offsetWidth;
    const children: HTMLCollection = this.contElem.nativeElement.children;

    if (children.length) {
      // get width of item in column
      this.colWidth = (children[0] as HTMLElement).offsetWidth;

      // get max num of columns that fit in container
      this.maxColumns = ~~(maxWidth / (this.colWidth + this.gap));

      // set column height array to empty
      this.columnHeights = [];

      // set elem column array to empty
      this.elemColumns = [];

      // fill arrays
      for (let i = 0; i < this.maxColumns; i++) {
        this.columnHeights[i] = 0;
        this.elemColumns[i] = [];
      }

      // set styles on items
      this.setElementsStyles(children);

      // set parent width/height
      this.contElem.nativeElement.style.width = this.getContainerWidth();
      this.contElem.nativeElement.style.height = `${
        Math.max(...this.columnHeights) + 40
      }px `;
    }
  }

  setElementsStyles(elements: HTMLCollection) {
    for (let i = 0; i < elements.length; i++) {
      const elem = elements[i] as HTMLElement;

      //set column to shortest column
      const columnTarget = this.columnHeights.indexOf(
        Math.min(...this.columnHeights)
      );

      // add elem to elem column
      this.elemColumns[columnTarget].push(elem);

      //set node postion
      const elemTop: number = this.columnHeights[columnTarget];
      const elemLeft: number =
        columnTarget * this.colWidth + columnTarget * this.gap;

      //  set css 3d transform
      elem.style.transform = `translateX(${elemLeft}px) translateY(${elemTop}px)`;

      //  add height of elem to column heights
      this.columnHeights[columnTarget] += elem.offsetHeight + this.gap;
    }
  }

  packOnlyColumn(num: number) {
    // reset col height
    this.columnHeights[num] = 0;

    for (let i = 0; i < this.elemColumns[num].length; i++) {
      const elem = this.elemColumns[num][i] as HTMLElement;

      //set node postion
      const elemTop: number = this.columnHeights[num];
      const elemLeft: number = num * this.colWidth + num * this.gap;

      //  set css 3d transform
      elem.style.transform = `translateX(${elemLeft}px)  translateY(${elemTop}px)`;

      //  add height of elem to column heights
      this.columnHeights[num] += elem.offsetHeight + this.gap;
    }
  }

  private getContainerWidth(): string {
    return `${
      this.colWidth * this.maxColumns + this.gap * (this.maxColumns - 1)
    }px`;
  }
}
