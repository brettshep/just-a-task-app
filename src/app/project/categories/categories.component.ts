import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
} from "@angular/core";
import { Project } from "../../../../interfaces";

@Component({
  selector: "project-categories",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="categories">
      <!-- all -->
      <div
        class="category"
        (click)="setCat.emit(null)"
        [class.active]="!currCatUID"
      >
        All
      </div>
      <!-- ng for -->
      <div
        class="category"
        *ngFor="let cat of project.categories"
        (click)="setCat.emit(cat.uid)"
        [class.active]="currCatUID === cat.uid"
      >
        >{{ cat.name }}
      </div>
    </div>
  `,
  styleUrls: ["./categories.component.sass"],
})
export class CategoriesComponent implements OnInit {
  @Input() currCatUID: string;
  @Input() project: Project;
  @Output() setCat = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}
}
