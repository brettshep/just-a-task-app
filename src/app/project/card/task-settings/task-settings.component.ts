import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from "@angular/core";
import { Member, Card, Project } from "../../../../../interfaces";

@Component({
  selector: "task-settings",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="cont">
      <i class="fas fa-cog settingsHandle" (click)="toggleSettings(true)"></i>
      <div
        *ngIf="settingsPopup"
        class="card"
        ClickedOutside
        (exit)="toggleSettings(false)"
      >
        <!-- assign section -->
        <section>
          <h4>Assign To</h4>
          <div class="grid">
            <img
              *ngFor="let member of members"
              class="gridItem"
              [src]="member.profilePic"
              alt="profile pic"
              (click)="assignMember(member.email)"
              [class.assigned]="assignedUID === member.email"
            />
          </div>
        </section>

        <!-- delete section -->
        <div class="border"></div>

        <div class="delete" (click)="delete.emit()">
          <h4>Delete <i class="fas fa-times-circle"></i></h4>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./task-settings.component.sass"],
})
export class TaskSettingsComponent {
  @Output() delete = new EventEmitter();
  @Output() assign = new EventEmitter<string>();
  @Input() members: Member[];
  @Input() assignedUID: string;
  // @Output() exit = new EventEmitter();
  // @Input() project: Project;
  // @Input() card: Card;
  // @Output() delete = new EventEmitter();
  // @Output() cardMemberAssign = new EventEmitter<string>();
  // assignCategory(uid: string) {
  //   const newUID = uid === this.card.categoryID ? null : uid;
  //   this.cardCategoryAssign.emit(newUID);
  //   this.exit.emit();
  // }

  assignMember(uid: string) {
    if (uid === this.assignedUID) {
      uid = null;
    }

    this.assign.emit(uid);
    this.settingsPopup = false;
  }

  toggleSettings(val: boolean) {
    this.settingsPopup = val;
  }

  settingsPopup: boolean;
}
