import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from "@angular/core";
import { Member, Card, Project } from "../../../../../interfaces";

@Component({
  selector: "card-settings",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card">
      <!-- assign section -->
      <section>
        <h4>Assign All To</h4>
        <div class="grid-img">
          <img
            *ngFor="let member of members"
            class="gridItem-img"
            [src]="member.profilePic"
            alt="profile pic"
            (click)="assignMember(member.email)"
          />
        </div>
        <!-- none -->
        <div class="assign-none" (click)="assignMember('')">None</div>
      </section>

      <!-- category section -->
      <div class="border"></div>

      <ng-container *ngIf="project.categories.length">
        <section>
          <h4>Category</h4>
          <div class="grid">
            <div
              class="gridItem icon"
              *ngFor="let cat of project.categories"
              [class.assigned]="card.categoryID === cat.uid"
              (click)="assignCategory(cat.uid)"
            >
              {{ cat.name }}
            </div>
          </div>
        </section>
      </ng-container>

      <!-- delete section -->
      <div class="border"></div>
      <div class="duplicate" (click)="duplicateCard()">
        <h4>Duplicate</h4>
      </div>

      <!-- delete section -->
      <div class="border"></div>

      <div class="delete" (click)="delete.emit()">
        <h4>Delete <i class="fas fa-times-circle"></i></h4>
      </div>
    </div>
  `,
  styleUrls: ["./card-settings.component.sass"],
})
export class CardSettingsComponent {
  @Input() project: Project;
  @Input() members: Member[];
  @Input() card: Card;
  @Output() cardCategoryAssign = new EventEmitter<string>();
  @Output() delete = new EventEmitter();
  @Output() duplicate = new EventEmitter();
  @Output() exit = new EventEmitter();
  @Output() cardMemberAssign = new EventEmitter<string>();

  assignCategory(uid: string) {
    const newUID = uid === this.card.categoryID ? null : uid;
    this.cardCategoryAssign.emit(newUID);
    this.exit.emit();
  }
  assignMember(uid: string) {
    this.cardMemberAssign.emit(uid);
    this.exit.emit();
  }
  duplicateCard() {
    this.duplicate.emit();
    this.exit.emit();
  }
}
