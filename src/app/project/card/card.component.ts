import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  ViewChild,
  inject,
} from "@angular/core";
import {
  Card,
  Task,
  Member,
  Project,
  CategoryList,
} from "../../../../interfaces";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";
import { ElementRef } from "@angular/core";
import { Firestore, collection, doc } from "@angular/fire/firestore";
import { CardUpdateType } from "../../../../interfaces";

@Component({
  selector: "card",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card cardPadding" cdkDrag>
      <!-- card title -->
      <div class="title">
        <input
          class="cardName"
          [value]="card.title"
          placeholder="Untitled Card"
          (keydown)="checkUpdateCardName($event)"
          (blur)="cardNameUpdate($event.target)"
        />
        <div class="iconPlace">
          <!-- card settings icon -->
          <i class="fas fa-grip-vertical dragHandle" cdkDragHandle></i>
          <span class="emoji options" (click)="toggleSettings(true)"
            ><i class="fas fa-cog"></i
          ></span>
        </div>

        <!-- card settings menu -->
        <card-settings
          class="cardSettings"
          *ngIf="cardSettingsPopup"
          [members]="members"
          [project]="project"
          [card]="card"
          ClickedOutside
          (cardCategoryAssign)="cardCategoryAssign($event)"
          (cardMemberAssign)="cardAssignMember($event)"
          (delete)="delete.emit(card)"
          (duplicate)="duplicate.emit(card)"
          (exit)="toggleSettings(false)"
        >
        </card-settings>
      </div>

      <!-- tasks -->
      <div
        class="taskList"
        cdkDropList
        [cdkDropListData]="card.tasks"
        cdkDropListLockAxis="y"
        cdkDropListOrientation="vertical"
        (cdkDropListDropped)="drop($event)"
      >
        <!-- ngfor tasks -->
        <div
          *ngFor="let task of card.tasks; index as i"
          class="task hover"
          [class.completed]="task.completed"
          cdkDrag
        >
          <i
            class="far fa-circle checkbox"
            (click)="toggleCompleteTask(i)"
            [ngStyle]="{
              color: task.completed ? project.bgColor : '',
              backgroundColor: task.completed ? project.bgColor : ''
            }"
          >
            <!-- checkmark -->
            <i
              class="fas fa-check checkmark"
              *ngIf="task.completed"
              [ngStyle]="{
              color: project.fontColor,
            }"
            ></i>
          </i>

          <div class="content-cover-cont">
            <div
              class="textarea"
              contenteditable
              customTextArea
              (onSubmit)="taskUpdate($event, task)"
            >
              {{ task.name }}
            </div>
            <div class="content-cover"></div>
          </div>
          <div class="empty"></div>

          <div class="task-rightside">
            <img
              class="assigned"
              *ngIf="task.assignedUserUID"
              [src]="findProfilePic(task.assignedUserUID)"
            />
            <div class="task-settings">
              <i class="fas fa-grip-vertical dragHandle" cdkDragHandle></i>
              <task-settings
                [members]="members"
                [assignedUID]="task.assignedUserUID"
                (delete)="deleteTask(task)"
                (assign)="assignTask($event, i)"
              />
            </div>
          </div>
        </div>
      </div>

      <!-- add task -->
      <div class="task addTask">
        <i #addTaskBtn class="far fa-plus-circle checkbox"></i>
        <div class="content-cover-cont">
          <div
            class="textarea"
            contenteditable
            customTextArea
            placeholder="Add a task..."
            [addTaskInput]="true"
            [addTaskBtn]="addTaskBtn"
            (onSubmit)="createTask($event)"
          ></div>
          <div class="content-cover"></div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./card.component.sass"],
})
export class CardComponent {
  @Input() card: Card;
  @Input() project: Project;
  @Input() members: Member[];
  @Input() categoryList: CategoryList;
  @Output() cardUpdate = new EventEmitter<{
    card: Card;
    calUpdate: CardUpdateType;
    task?: Task;
  }>();
  @Output() delete = new EventEmitter<Card>();
  @Output() duplicate = new EventEmitter<Card>();

  constructor(
    private elemRef: ElementRef<HTMLElement>,
    private fireStore: Firestore = inject(Firestore)
  ) {}

  // popups
  cardSettingsPopup: boolean;

  findProfilePic(email: string) {
    return this.members.find((member) => member.email === email).profilePic;
  }

  // ----CARD UPDATES----
  taskUpdate(e: HTMLDivElement, task: Task) {
    const taskName = e.textContent;
    const index = this.card.tasks.findIndex((_task) => _task.uid === task.uid);
    this.card.tasks[index].name = taskName;
    this.cardUpdate.emit({
      card: this.card,
      calUpdate: "tasksEdit",
      task: this.card.tasks[index],
    });
  }

  cardNameUpdate(target: EventTarget) {
    const name = (target as HTMLInputElement).value.trim();
    if (name !== this.card.title) {
      this.card.title = name;
      this.cardUpdate.emit({ card: this.card, calUpdate: "title" });
    }
  }

  createTask(e: HTMLDivElement) {
    const taskName = e.textContent;
    const newTask: Task = {
      uid: doc(collection(this.fireStore, "test")).id,
      name: taskName,
      completed: false,
    };
    this.card.tasks = [...this.card.tasks, newTask];

    // clear input
    e.textContent = "";

    this.cardUpdate.emit({ card: this.card, calUpdate: "tasksCreate" });
  }

  cardCategoryAssign(uid: string) {
    this.card.categoryID = uid;

    this.cardUpdate.emit({ card: this.card, calUpdate: "categoryID" });
  }

  cardAssignMember(uid: string) {
    if (!this.card.tasks?.length) return;

    for (const task of this.card.tasks) {
      task.assignedUserUID = uid;
    }

    this.cardUpdate.emit({ card: this.card, calUpdate: "cardAssignUser" });
  }

  assignTask(uid: string, taskIndex: number) {
    this.card.tasks[taskIndex].assignedUserUID = uid;

    this.cardUpdate.emit({
      card: this.card,
      calUpdate: "assignedUserUID",
      task: this.card.tasks[taskIndex],
    });
  }

  deleteTask(task: Task) {
    // renmove task from card
    const taskToDelete = task;
    this.card.tasks = this.card.tasks.filter((task) => task !== taskToDelete);
    this.cardUpdate.emit({
      card: this.card,
      calUpdate: "tasksDelete",
      task: taskToDelete,
    });
  }

  toggleCompleteTask(taskIndex: number) {
    // renmove task from card
    this.card.tasks[taskIndex].completed =
      !this.card.tasks[taskIndex].completed;

    this.cardUpdate.emit({
      card: this.card,
      calUpdate: "tasksComplete",
      task: this.card.tasks[taskIndex],
    });
  }

  drop(event: CdkDragDrop<Task[]>) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );

    this.cardUpdate.emit({ card: this.card, calUpdate: "tasksOrder" });
  }

  // ----POPUPS/CHECKS------

  checkUpdateCardName(e: KeyboardEvent) {
    if (e.key === "Enter") {
      (e.target as HTMLInputElement).blur();
    }
  }

  toggleSettings(bool: boolean) {
    this.cardSettingsPopup = bool;
    if (bool) {
      this.elemRef.nativeElement.classList.add("settingsOpen");
    } else {
      this.elemRef.nativeElement.classList.remove("settingsOpen");
    }
  }
}
