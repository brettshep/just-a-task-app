import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

import { DragDropModule } from "@angular/cdk/drag-drop";
import { SharedModule } from "../shared/shared.module";
import { ScrollingModule } from "@angular/cdk/scrolling";

import { ProjectComponent } from "./project.component";
import { CategoriesComponent } from "./categories/categories.component";
import { CardComponent } from "./card/card.component";
import { LayoutDirective } from "./layout.directive";
import { CardSettingsComponent } from "./card/card-settings/card-settings.component";
import { TaskSettingsComponent } from "./card/task-settings/task-settings.component";
import { ProjectSettingsPopupComponent } from "./project-settings-popup/project-settings-popup.component";

export const ROUTES: Routes = [
  {
    path: "",
    component: ProjectComponent,
  },
  { path: "**", redirectTo: "/home" },
];

@NgModule({
  declarations: [
    ProjectComponent,
    CategoriesComponent,
    CardComponent,
    LayoutDirective,
    CardSettingsComponent,
    TaskSettingsComponent,
    ProjectSettingsPopupComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    DragDropModule,
    SharedModule,
    ScrollingModule,
  ],
})
export class ProjectModule {}
