import { Component, OnInit } from "@angular/core";
import {
  Project,
  Card,
  Task,
  Member,
  CategoryList,
  CardUpdateType,
  Workspace,
} from "../../../interfaces";
import {
  Subject,
  fromEvent,
  Subscription,
  Observable,
  BehaviorSubject,
} from "rxjs";
import { throttleTime, map } from "rxjs/operators";
import { ProjectService } from "../services/project.service";
import { combineLatest } from "rxjs";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";

@Component({
  selector: "app-project",
  template: `
    <div
      class="container"
      [class.noScroll]="projectSettingsModal"
      *ngIf="currProject$ | async as currentProj; else loading"
    >
      <!-- Project Title Header -->
      <header>
        <div class="space calendar">
          <i
            class="far fa-ellipsis-h"
            (click)="projectSettingsPopup = true"
          ></i>

          <!-- settings menu popup -->
          <project-settings-popup
            *ngIf="projectSettingsPopup"
            ClickedOutside
            (exit)="projectSettingsPopup = false"
            (openSettings)="openProjectSettings()"
            (deleteProject)="deleteProject(currentProj)"
          ></project-settings-popup>
        </div>

        <!-- project Selection -->
        <div class="text" *ngIf="workspace$ | async as workspace">
          <h1>{{ workspace.name }}</h1>
        </div>
      </header>

      <!-- projects -->
      <div *ngIf="projects$ | async as projects" class="projects-menu">
        <!-- ng for -->
        <div
          class="project"
          *ngFor="let project of projects"
          (click)="setCurrentProject(project)"
          [class.active]="currentProj === project"
          [ngStyle]="{
            background: currentProj === project ? project.bgColor : '',
            color: currentProj === project ? project.fontColor : ''
          }"
        >
          {{ project.name }}
        </div>
      </div>

      <!-- Rest Of Screen -->
      <ng-container *ngIf="categoryList$ | async as categoryList" class="card">
        <!-- cards -->
        <div
          *ngIf="cardGroups$ | async as groups"
          #cardContainer
          cdkScrollable
          class="card-container"
          DragScroll
          [validClasses]="draggableClasses"
        >
          <div
            class="card-group"
            [class.pointer-none]="currSelectedUser$ | async"
            *ngFor="let group of groups"
            cdkDropList
            cdkDropListLockAxis="y"
            cdkDropListOrientation="vertical"
            [cdkDropListData]="group.cards"
            (cdkDropListDropped)="drop($event, currentProj)"
          >
            <h2>
              <span>
                {{ categoryList[group.categoryID]?.name ?? "Uncategorized" }}
              </span>
              <button
                class="add-card-btn"
                (click)="addNewCard(currentProj, group.categoryID)"
              >
                <i class="fas fa-plus"></i>
              </button>
            </h2>
            <card
              *ngFor="let card of group.cards"
              [card]="card"
              [project]="currentProj"
              [members]="members$ | async"
              [categoryList]="categoryList"
              (cardUpdate)="cardUpdate($event, currentProj)"
              (delete)="cardDelete($event, currentProj)"
              (duplicate)="cardDuplicate($event, currentProj)"
            ></card>
          </div>
        </div>
      </ng-container>

      <!-- project settings popup -->
      <project-settings
        *ngIf="projectSettingsModal"
        [project]="currentProj"
        (exit)="projectSettingsModal = false"
        (editProject)="editProjectSettings($event)"
        (deleteProjectCategory)="deleteProjectCategory($event, currentProj)"
      ></project-settings>
    </div>
    <ng-template #loading>
      <div class="spinner"></div>
    </ng-template>
  `,
  styleUrls: ["./project.component.sass"],
})
export class ProjectComponent implements OnInit {
  repack$ = new Subject();
  resizeSub: Subscription;

  //popups
  projectSettingsPopup: boolean = false;
  projectSettingsModal: boolean = false;

  // data observables
  currSelectedUser$: BehaviorSubject<string>;
  workspace$: Observable<Workspace>;
  projects$: Observable<Project[]>;
  currProject$: Observable<Project>;
  members$: Observable<Member[]>;
  categoryList$: Observable<CategoryList>;
  cardGroups$: Observable<{ cards: Card[]; categoryID: string }[]>;

  draggableClasses = ["card-container", "card-group"];

  constructor(private projectServ: ProjectService) {}

  ngOnInit(): void {
    this.workspace$ = this.projectServ.currWorkspace$;

    // fetch projects
    this.projects$ = this.projectServ.fetchProjects();

    // set current project
    this.currProject$ = this.projectServ.currProject$;

    // get memebers form current workspace
    this.members$ = this.projectServ.currWorkspace$.pipe(
      map((workspace) => {
        if (workspace) {
          let members = [];
          for (const key in workspace.members) {
            if (workspace.members.hasOwnProperty(key)) {
              members.push(workspace.members[key]);
            }
          }
          return members;
        } else {
          return [];
        }
      })
    );

    this.currSelectedUser$ = this.projectServ.currSelectedUser$;

    // create category list
    this.categoryList$ = this.currProject$.pipe(
      map((project) => {
        let list: CategoryList = {};
        if (project) {
          project.categories.forEach((cat) => {
            list[cat.uid] = cat;
          });
        }

        return list;
      })
    );

    this.cardGroups$ = combineLatest([
      this.projectServ.fetchCards(),
      this.currProject$,
      this.projectServ.currSelectedUser$,
    ]).pipe(
      map(([_cards, project, selectedUser]) => {
        if (!_cards || !project) return [];

        let cards = [..._cards];

        const groupsObj: Record<string, Card[]> = {};
        const ungrouped = cards.filter((card) => !card.categoryID);

        for (const key in project.categories) {
          const cat = project.categories[key];
          groupsObj[cat.uid] = [];
        }

        cards.forEach((_card) => {
          const card = { ..._card };

          const catUID = card.categoryID;
          if (selectedUser) {
            card.tasks = card.tasks.filter(
              (task) => task.assignedUserUID === selectedUser
            );

            if (card.tasks.length && groupsObj[catUID]) {
              groupsObj[catUID].push(card);
            }
          } else {
            if (groupsObj[catUID]) groupsObj[catUID].push(card);
          }
        });

        //sort the cards
        for (const key in groupsObj) {
          groupsObj[key].sort((a, b) => a.order - b.order);
          ungrouped.sort((a, b) => a.order - b.order);
        }

        const groups = Object.entries(groupsObj).map(
          ([categoryID, catCards]) => {
            return { cards: catCards.filter((c) => !!c), categoryID };
          }
        );

        if (ungrouped.length) {
          groups.unshift({ cards: ungrouped, categoryID: "" });
        }

        return groups;
      })
    );

    this.resizeSub = fromEvent(window, "resize")
      .pipe(throttleTime(100))
      .subscribe(() => {
        this.repack$.next(null);
      });
  }

  ngOnDestroy() {
    this.resizeSub.unsubscribe();
  }

  setCurrentProject(e: Project) {
    // set next project
    this.projectServ.setCurrentProject(e);
  }

  deleteProject(proj: Project) {
    this.projectServ.deleteProject(proj);
  }

  // -------CARD CRUD---------

  drop(event: CdkDragDrop<Card[]>, proj: Project) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );

    this.projectServ.updateCardsOrder(proj, event.container.data);
  }

  // create
  addNewCard(proj: Project, categoryID: string) {
    this.projectServ.createCard(proj, categoryID);
  }

  // update
  cardUpdate(
    e: {
      card: Card;
      calUpdate: CardUpdateType;
      task?: Task;
    },
    project: Project
  ) {
    this.projectServ.updateCard(e.card, project, e.calUpdate, e.task);
  }

  // delete
  cardDelete(card: Card, project: Project) {
    this.projectServ.deleteCard(card, project);
  }

  // duplicate
  cardDuplicate(card: Card, project: Project) {
    this.projectServ.duplicateCard(card, project);
  }

  //delete project category and cards
  deleteProjectCategory(
    e: { deleteUID: string; moveToUID: string },
    project: Project
  ) {
    // delete category
    this.projectServ.deleteProjectCategory(e.deleteUID, e.moveToUID, project);
  }

  // ----Project Settings updates an Popup-----

  editProjectSettings(project: Project) {
    this.projectSettingsModal = false;
    this.projectServ.updateProject(project);
  }

  openProjectSettings() {
    this.projectSettingsModal = true;
  }

  // ---------Repacking---------

  repack() {
    this.repack$.next(null);
  }

  // ------Track Function-------
  // cardTrackByFunction(index: number, card: Card): string {
  //   return card.uid;
  // }
}
