// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'just-a-task-app',
    appId: '1:183499448188:web:64ea2a4575587eeed04ef9',
    storageBucket: 'just-a-task-app.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyBi8l1yqp67ZTa_eICpEw2lymvo88Ir4_0',
    authDomain: 'just-a-task-app.firebaseapp.com',
    messagingSenderId: '183499448188',
    measurementId: 'G-Z9NGR9RKWJ',
  },
  production: false
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
