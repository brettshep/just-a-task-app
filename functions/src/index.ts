import * as functions from "firebase-functions";
import * as sendGrid from "@sendgrid/mail";

interface InviteEmailData {
  to: string;
  from: string;
  workspace: string;
  inviteUID: string;
}

const apiKey =
  "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";

const templateId = "d-ccca12b44d6540ffae902401d03b09da";

sendGrid.setApiKey(apiKey);

export const sendEmail = functions.https.onCall(
  async (data: InviteEmailData, ctx) => {
    const msg: sendGrid.MailDataRequired = {
      to: data.to,
      from: "hello@brandon.design",
      templateId,
      dynamicTemplateData: {
        name: data.from,
        workspace: data.workspace,
        link: `https://just-a-task-app.web.app/invite/${data.inviteUID}`,
      },
    };
    try {
      await sendGrid.send(msg);
      console.log("sent to: ", data.to);
      return { success: true };
    } catch (error) {
      return { error };
    }
  }
);
